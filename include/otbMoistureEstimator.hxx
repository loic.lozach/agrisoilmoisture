/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef otbMoistureEstimator_txx
#define otbMoistureEstimator_txx

#include "otbMoistureEstimator.h"

namespace otb
{

template <class TInputMap, class TOutputMap>
MoistureEstimator<TInputMap, TOutputMap>
::MoistureEstimator()
 {
  m_BatchSize = 1;
  m_NumberOfBands = 3;
  
  m_OutputTensorName = "dense_3/BiasAdd";
  m_InputPlaceholder = "dense_1_input";
 }

template<class TInputMap, class TOutputMap>
tensorflow::Tensor
MoistureEstimator<TInputMap, TOutputMap>
::CreateNewTensor(unsigned int nElem)
 {
  unsigned int movingBatchSize (nElem);
  if (m_InputMap.size() < movingBatchSize)
    movingBatchSize = m_InputMap.size() ;
  tensorflow::TensorShape inputTensorShape({movingBatchSize, m_NumberOfBands});
  return tensorflow::Tensor(tensorflow::DT_FLOAT, inputTensorShape);
 }

/**
 * Do the work
 */
template <class TInputMap, class TOutputMap>
void
MoistureEstimator<TInputMap, TOutputMap>
::Update()
 {

  // Check number of inputs
  if (m_InputMap.size() == 0)
  {
    itkExceptionMacro("Input map is empty");
  }

  // Number of batches
  const unsigned int nBatches = vcl_ceil(m_InputMap.size() / m_BatchSize);
  const unsigned int rest = m_InputMap.size() % m_BatchSize;

  // Progress reported
  itk::ProgressReporter progress(this, 0, nBatches);

  // Create an empty tensor
  tensorflow::Tensor inputTensor = CreateNewTensor(m_BatchSize);

  // Prepare input labels
  std::vector<unsigned int> labels;
  labels.reserve(m_BatchSize);

  // Iterate over the input map
  unsigned int curSample = 0;
  unsigned int curBatch = 0;
  for (const auto& entry: m_InputMap)
  {

    // Fill the tensor with the current map entry
    auto tMap = inputTensor.tensor<float, 2>();
    for (unsigned int band = 0 ; band < m_NumberOfBands ; band++)
    {
      tMap(curSample, band) = static_cast<float>(entry.second[band]);
    }
    // Fill the labels
    labels.push_back(entry.first);

    // Once the last sample of the batch is reached, run the session
    curSample++;
    if (curSample == inputTensor.dim_size(0)) // The batch dim
    {
      // Update current batch
      curSample = 0;
      curBatch++;

      // Run session
      TensorListType outputs;
      DictElementType dict;
      dict.first = m_InputPlaceholder;
      dict.second = inputTensor;
      auto status = m_Session->Run({dict}, {m_OutputTensorName}, {}, &outputs);

      if (!status.ok()) {

        // Throw an exception with the report
        itkExceptionMacro("Can't run the tensorflow session !\n" <<
            "Tensorflow error message:\n" << status.ToString()  );

      }

      // Recopy output in map
      if (outputs.size() == 0)
        itkExceptionMacro("Output tensor size is null")

      auto tFlat = outputs[0].flat<float>();
      for (unsigned int elem = 0 ; elem < outputs[0].NumElements() ; elem++)
        m_OutputMap[labels[elem]] = tFlat(elem);

      // Empty labels
      labels.clear();
      labels.reserve(m_BatchSize);

      // Update progress
      progress.CompletedPixel();

      // Verify the last batch size
      if (rest != 0 && curBatch == nBatches - 1)
      {
        // If the last batch size is not
        inputTensor = CreateNewTensor(rest);
      }

    } // Next map entry
  }


 }

} // end namespace otb


#endif
