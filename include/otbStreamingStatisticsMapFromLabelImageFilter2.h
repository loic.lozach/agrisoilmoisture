/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   otbStreamingStatisticsMapFromLabelImageFilter2.h
 * Author: otbuser
 *
 * Created on January 14, 2021, 12:54 AM
 */

#ifndef OTBSTREAMINGSTATISTICSMAPFROMLABELIMAGEFILTER2_H
#define OTBSTREAMINGSTATISTICSMAPFROMLABELIMAGEFILTER2_H

#include "otbPersistentImageFilter.h"
#include "itkNumericTraits.h"
#include "itkArray.h"
#include "itkSimpleDataObjectDecorator.h"
#include "otbPersistentFilterStreamingDecorator.h"
#include <unordered_map>

namespace otb
{
 
template <class TInputVectorImage, class TLabelImage>
class ITK_EXPORT PersistentStreamingStatisticsMapFromLabelImageFilter2 : public PersistentStreamingStatisticsMapFromLabelImageFilter<TInputVectorImage, TInputVectorImage>
{
public:
  /** Standard Self typedef */
  typedef PersistentStreamingStatisticsMapFromLabelImageFilter2 Self;
  typedef PersistentStreamingStatisticsMapFromLabelImageFilter<TInputVectorImage, TInputVectorImage> Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Runtime information support. */
  itkTypeMacro(PersistentStreamingStatisticsMapFromLabelImageFilter2, PersistentStreamingStatisticsMapFromLabelImageFilter);
  
  typedef typename LabelImageType::PixelType             LabelPixelType;
  typedef itk::VariableLengthVector<double>              RealVectorValidPixelsRatioType;
  typedef std::unordered_map<LabelPixelType, RealVectorValidPixelsRatioType> VectorValidPixelsRatioMapType;
  
  /** Return the ratio between valid pixels for each band and pixels in each label */
  VectorValidPixelsRatioMapType GetValidPixelsRatioMap() const;
  
  void Synthetize(void) override;
  
private:
      
  VectorValidPixelsRatioMapType m_CountPixel;
};
   
template <class TInputVectorImage, class TLabelImage>
class ITK_EXPORT StreamingStatisticsMapFromLabelImageFilter2
    : public PersistentFilterStreamingDecorator<PersistentStreamingStatisticsMapFromLabelImageFilter2<TInputVectorImage, TLabelImage>>
{
public:
  /** Standard Self typedef */
  typedef StreamingStatisticsMapFromLabelImageFilter2 Self;
  typedef PersistentFilterStreamingDecorator<PersistentStreamingStatisticsMapFromLabelImageFilter2<TInputVectorImage, TLabelImage>> Superclass;
  typedef itk::SmartPointer<Self>       Pointer;
  typedef itk::SmartPointer<const Self> ConstPointer;

  /** Type macro */
  itkNewMacro(Self);

  /** Creation through object factory macro */
  itkTypeMacro(StreamingStatisticsMapFromLabelImageFilter2, StreamingStatisticsMapFromLabelImageFilter);
  
  typedef typename Superclass::FilterType::VectorValidPixelsRatioMapType       VectorValidPixelsRatioMapType;

  /** Return the ratio between valid pixels for each band and pixels in each label */
  VectorValidPixelsRatioMapType GetValidPixelsRatioMap() const
  {
    return this->GetFilter()->GetValidPixelsRatioMap();
  }
    
};
}
#ifndef OTB_MANUAL_INSTANTIATION
#include "otbStreamingStatisticsMapFromLabelImageFilter2.hxx"
#endif

#endif /* OTBSTREAMINGSTATISTICSMAPFROMLABELIMAGEFILTER2_H */

