/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   otbSentinel2Functor.h
 * Author: workshop
 *
 * Created on March 12, 2019, 6:31 PM
 */


#ifndef OTBSENTINEL2FUNCTOR_H
#define OTBSENTINEL2FUNCTOR_H

namespace otb
{
    namespace Functor
    {
        


template<class InputVectorPixel, class OutputPixel>
class TimeSerieMeanFunctor
{
    public:
        TimeSerieMeanFunctor(){}
        ~TimeSerieMeanFunctor(){}
        
        inline OutputPixel operator ()(const InputVectorPixel& input) const
        {
//            unsigned int numberOfBands = sizeof(input);
//            std::vector<float> validpix;
//            
//            for(unsigned int band = 0 ; band < numberOfBands ; band++){
//                if ( input[band] > 0)
//                    validpix.push_back(input[band]);
//                
//            }
            
//            float sum = 0;
//            for (float p:validpix){
//
//                sum = sum + p;
//             }
            
            float sum = 0;
            int i = 0;
            for (unsigned int p = 0; p < input.Size(); p++){
                if (input[p] > 0){
                    sum = sum + input[p];
                    i++;
                }
             }
            float moy=0;
            if (i > 0){
                moy = sum/i;
            }
             return moy;
        }
};

    }
}

#endif /* OTBSENTINEL2FUNCTOR_H */

