/*=========================================================================

  Copyright (c) Remi Cresson (IRSTEA). All rights reserved.


     This software is distributed WITHOUT ANY WARRANTY; without even
     the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
     PURPOSE.  See the above copyright notices for more information.

=========================================================================*/
#ifndef otbMoistureEstimator_h
#define otbMoistureEstimator_h

#include "itkProcessObject.h"
#include "itkNumericTraits.h"
#include "itkSimpleDataObjectDecorator.h"

// Tensorflow
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/platform/env.h"

// Tensorflow helpers
#include "otbTensorflowGraphOperations.h"
#include "otbTensorflowDataTypeBridge.h"
#include "otbTensorflowCopyUtils.h"
#include "otbTensorflowCommon.h"

namespace otb
{

/**
 * \class MoistureEstimator
 * \brief This process objects performs the inversion of IEM model.
 *
 * \ingroup ground-humidity
 */
template <class TInputMap, class TOutputMap>
class ITK_EXPORT MoistureEstimator :
public itk::ProcessObject
{
public:

  /** Standard class typedefs. */
  typedef MoistureEstimator                       Self;
  typedef itk::ProcessObject                      Superclass;
  typedef itk::SmartPointer<Self>                 Pointer;
  typedef itk::SmartPointer<const Self>           ConstPointer;

  /** Method for creation through the object factory. */
  itkNewMacro(Self);

  /** Run-time type information (and related methods). */
  itkTypeMacro(MoistureEstimator, itk::ProcessObject);

  /** Typedefs for parameters */
  typedef std::pair<std::string, tensorflow::Tensor> DictElementType;
  typedef std::vector<tensorflow::Tensor>            TensorListType;

  /** Do the real work */
  virtual void Update();

  itkSetMacro(InputMap, TInputMap);
  itkGetMacro(InputMap, TInputMap);

  /** Get outputs */
  itkGetMacro(OutputMap, TOutputMap);

  itkSetMacro(BatchSize, unsigned long);
  itkGetMacro(BatchSize, unsigned long);

  /** Set and Get the Tensorflow session and graph */
  void SetGraph(tensorflow::GraphDef graph)      { m_Graph = graph;     }
  tensorflow::GraphDef GetGraph()                { return m_Graph ;     }
  void SetSession(tensorflow::Session * session) { m_Session = session; }
  tensorflow::Session * GetSession()             { return m_Session;    }

protected:
  MoistureEstimator();
  virtual ~MoistureEstimator() {};

  tensorflow::Tensor CreateNewTensor(unsigned int nElem);

private:
  MoistureEstimator(const Self&); //purposely not implemented
  void operator=(const Self&); //purposely not implemented

  TInputMap m_InputMap;
  TOutputMap m_OutputMap;
  unsigned int m_BatchSize;
  unsigned int m_NumberOfBands;

  std::string m_OutputTensorName;
  std::string m_InputPlaceholder;

  // Tensorflow graph and session
  tensorflow::GraphDef       m_Graph;                   // The TensorFlow graph
  tensorflow::Session *      m_Session;                 // The TensorFlow session

}; // end class

} // end namespace otb

#include "otbMoistureEstimator.hxx"

#endif
