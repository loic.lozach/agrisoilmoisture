/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   otbHumidityFunctors.h
 * Author: llozach
 *
 * Created on 27 aout 2018, 15:08
 */

#ifndef OTBHUMIDITYFUNCTORS_H
#define OTBHUMIDITYFUNCTORS_H
namespace otb
{
    namespace Functor
    {
        
template<class InputPixel, class OutputPixel>
class ThresholdFunctor
{
    public:
        ThresholdFunctor(){}
        ~ThresholdFunctor(){}
        
        inline OutputPixel operator ()(const InputPixel& input) const
        {
            if (input != 0 )
                return 0;
            return 1;
        }
};

template<class InputPixel, class OutputPixel>
class MaskFunctor
{
    public:
        MaskFunctor(){}
        ~MaskFunctor(){}
        
        inline OutputPixel operator ()(const InputPixel& input) const
        {
            if (input > 0 )
                return 1;
            return 0;
        }
};

template<class InputPixel, class OutputPixel>
class MaskLULCFunctor
{
    private:
        std::vector<int> m_Agrivalues;
        
    public:
        MaskLULCFunctor(){}
        ~MaskLULCFunctor(){}
        
        inline OutputPixel operator ()(const InputPixel& input) const
        {
            int pix = static_cast<int>(input);
                    
            if(std::find(m_Agrivalues.begin(), m_Agrivalues.end(), pix ) != m_Agrivalues.end()) 
                return 1;
            return 0;
        }
        
        void SetAgrivalues(std::vector<std::string> agrivalues){
            
//            for(std::vector<std::string>::const_iterator i =  agrivalues.begin(); i !=  agrivalues.end(); ++i ) {
//                this->agrivalues.push_back(std::atoi(agrivalues[i]));
//            }
            for(const auto& i :  agrivalues) {
                m_Agrivalues.push_back(std::stoi(i));
            }

//            std::cout << this->agrivalues[0];
//            std::cout << this->agrivalues[1];
        }
};

template<class SARVVPixelType,class SARTHPixelType>
class SarAmplituteMaxFunctor
{
    public:
        SarAmplituteMaxFunctor(){}
        ~SarAmplituteMaxFunctor(){}
        
        inline SARVVPixelType operator ()(const SARVVPixelType& input1,const SARTHPixelType& input2) const
        {
            SARVVPixelType output(input1);
            float vvmax = (-0.00002563*100*100*input2[0]*input2[0] +  0.05859124*100*input2[0] + 604.97)/1000;
            if (input1[0] < 0.0031627766 or input1[0] > vvmax ){
                output.Fill(0);
            }
            return output;
        }
};

template<class SARVHPixelType,class SARTHPixelType>
class SarVHAmplituteMaxFunctor
{
    public:
        SarVHAmplituteMaxFunctor(){}
        ~SarVHAmplituteMaxFunctor(){}
        
        inline SARVHPixelType operator ()(const SARVHPixelType& input1,const SARTHPixelType& input2) const
        {
            SARVHPixelType output(input1);
            float vhmax = (-0.00000828*100*100*input2[0]*input2[0] +  0.02760401*100*input2[0] + 320.86)/1000;
            if (input1[0] < 0.002 or input1[0] > vhmax ){
                output.Fill(0);
            }
            return output;
        }
};

template<class MVPixelType,class NDVIPixelType>
class NdviAndMoistFunctor
{
    public:
        NdviAndMoistFunctor(){}
        ~NdviAndMoistFunctor(){}
        
        inline MVPixelType operator ()(const MVPixelType& input1,const NDVIPixelType& input2) const
        {
            //input1 = MoistMap , input2 = NDVI
            MVPixelType output(input1);
            if (input1[0] <= 7 and input2[0] > 0.7 ){
                output.Fill(0);
            }
            return output;
        }
};


template<class InputPixel1,class InputPixel2, class OutputPixel>
class NDVIFunctor
{
    public:
        NDVIFunctor(){}
        ~NDVIFunctor(){}
        
        inline OutputPixel operator ()(const InputPixel1& red,const InputPixel2& nir) const
        {
            
                    
            return static_cast<OutputPixel>((nir+red)==0?0:(nir-red)/(nir+red));
        }
};

/*
 * This functor inputs a label value and produce an output value
 * using the map passed by reference (if the label is not in the
 * map, it returns 0. Else, it returns the value of the label in
 * the map)
 */
template<class LabelValue, class OutputValue, class TMapType>
class MapperFunctor
{
public:
  MapperFunctor(){}
  ~MapperFunctor(){}

  void SetMap(TMapType mapref)
  {
    m_Map = mapref;
  }

  inline OutputValue operator ()(const LabelValue& input) const
  {
    if (m_Map.count(input) > 0)
      return m_Map.at(input);
    return 0;
  }

private:
  TMapType m_Map;
};


   
    }
}



#endif /* OTBHUMIDITYFUNCTORS_H */

