#!/usr/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import os
import argparse
import otbApplication

def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit
            
    return images

def ex_pipeline(argsformat, extr, ref, out):
    
    print("processing.......................................................")
    print("image = "+extr)
    print("output = "+out)
    
    app0 = otbApplication.Registry.CreateApplication("ExtractROI")
    app0.SetParameterString("in",extr)
    app0.SetParameterString("out", "temp0.tif")
    app0.SetParameterString("mode","fit")
    app0.SetParameterString("mode.fit.im",ref)
    app0.Execute()
    
    app3 = otbApplication.Registry.CreateApplication("ManageNoData")
    app3.SetParameterInputImage("in", app0.GetParameterOutputImage("out"))
    app3.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    if argsformat == 'L8-NDVI':
        app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    elif argsformat == 'S1-SNAP':
        app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    else:
        print("error!")
        exit
    app3.SetParameterString("mode", "changevalue")
    
    app3.ExecuteAndWriteOutput()
    
    print("done.............................................................")
    
def exmos_pipeline(argsformat, mos1, mos2, ref, out):
    
    print("processing.......................................................")
    print("image1 = "+mos1)
    print("image2 = "+mos2)
    print("output = "+out)
    
    app0 = otbApplication.Registry.CreateApplication("Superimpose")
    app0.SetParameterString("inm",mos1)
    app0.SetParameterString("out", "temp0.tif")
    app0.SetParameterString("interpolator","nn")
    app0.SetParameterString("inr",ref)
    app0.Execute()
    
    app1 = otbApplication.Registry.CreateApplication("Superimpose")
    app1.SetParameterString("inm",mos2)
    app1.SetParameterString("out", "temp1.tif")
    app1.SetParameterString("interpolator","nn")
    app1.SetParameterString("inr",ref)
    app1.Execute()
    
    app2 = otbApplication.Registry.CreateApplication("Mosaic")
    app2.AddImageToParameterInputImageList("il",app0.GetParameterOutputImage("out"))
    app2.AddImageToParameterInputImageList("il", app1.GetParameterOutputImage("out"))
    app2.SetParameterString("out", "temp2.tif")
    app2.Execute()
    
    app3 = otbApplication.Registry.CreateApplication("ManageNoData")
    app3.SetParameterInputImage("in", app2.GetParameterOutputImage("out"))
    app3.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    if argsformat == 'L8-NDVI':
        app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    elif argsformat == 'S1-SNAP':
        app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    else:
        print("error!")
        exit
    app3.SetParameterString("mode", "changevalue")
    
    app3.ExecuteAndWriteOutput()
    
    print("done.............................................................")

if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Extract and mosaic Sentinel-1 or Landsat8 to fit Sentinel-2 Tile
        """)
    
    parser.add_argument('-format', choices=['S1-SNAP','L8-NDVI'], required=True)
    parser.add_argument('-mode', choices=['vv','vh'], required=True, default="vv")
    parser.add_argument('-indir', action='store', required=True, help='Directory containing Sentinel1 SNAP Calibrated or Landsat8 NDVI')
    parser.add_argument('-inref', action='store', required=True, help='Image references Sentinel-2 Tile, example any S2 NDVI')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')
#    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
#    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    # parser
    
    args=parser.parse_args()
    
    if not os.path.isdir(args.outdir):
        os.mkdir(args.outdir)
    
    abspathout = os.path.abspath(args.outdir)
    splitinref = os.path.basename(args.inref).split("_")
    tileid = splitinref[1]
    
    if args.mode == "vv":
        modeinprefix = "Sigma0_VV.img"
        modeoutprefix = "_VV.TIF"
    elif args.mode == "vh":
        modeinprefix = "Sigma0_VH.img"
        modeoutprefix = "_VH.TIF"
    else:
        print("Error: mode not recognized")
        exit()
    
    if args.format == "S1-SNAP" :
        
        indir=[]
        indir=search_files(args.indir, 'S1', 'data', 'd')
        
        dejafait=[]
        for file in indir:
            if file in dejafait:
                continue
            bname = os.path.basename(file).split(".")[0]
            splitbname = bname.split("_")
            acqdate = splitbname[4].split("T")[0]  
            acqtime = splitbname[4].split("T")[1]
            
            
            outfilebase = os.path.join(abspathout,"_".join(splitbname[:3])+"_"+tileid+"_"+acqdate+"T"+acqtime)
           
            l = [indir.index(i) for i in indir if acqdate in i]
            
            
            if len(l) == 2 :
                in1 = os.path.join(indir[l[0]], modeinprefix)
                in2 = os.path.join(indir[l[1]], modeinprefix)

                outfile = outfilebase +modeoutprefix
                
                if os.path.exists(outfile) :        
                    continue
                
                exmos_pipeline(args.format, in1, in2, args.inref, outfile)
                
                in1 = os.path.join(indir[l[0]], "incidenceAngleFromEllipsoid.img")
                in2 = os.path.join(indir[l[1]], "incidenceAngleFromEllipsoid.img")

                outfile = outfilebase +"_THETA.TIF"
                
                exmos_pipeline(args.format, in1, in2, args.inref, outfile)
                
                dejafait.append(indir[l[0]])
                dejafait.append(indir[l[1]])
                
            elif len(l) == 1 :
                in1 = os.path.join(file, modeinprefix)
                outfile = outfilebase +modeoutprefix
                
                if os.path.exists(outfile) :        
                    continue
                
                ex_pipeline(args.format, in1, args.inref, outfile)
                
                in1 = os.path.join(file, "incidenceAngleFromEllipsoid.img")
                outfile = outfilebase +"_THETA.TIF"
                
                ex_pipeline(args.format, in1, args.inref, outfile)
                
            else :
                print("Error on index :"+l)
                
            
        
    elif args.format == "L8-NDVI" :
        
        indir=[]
        indir=search_files(args.indir, 'NDVI_LC08', 'tif', 'f')
        
        for file in indir:
            bname = os.path.basename(file).split(".")[0]
            splitbname = bname.split("_")
            acqdate = splitbname[-1]       
                
            outfile = os.path.join(abspathout,"NDVI_"+tileid+"_"+acqdate+"T000000_"+splitbname[1]+".TIF")
            if os.path.exists(outfile) :        
                    continue
            
            l = [indir.index(i) for i in indir if acqdate in i]
            
            if len(l) == 2 :
                
                exmos_pipeline(args.format, indir[l[0]], indir[l[1]], args.inref, outfile)
                
            elif len(l) == 1 :
                
                ex_pipeline(args.format, file, args.inref, outfile)
                
            else :
                print("Error on index :"+l)
    
        
    else:
        print("S2 format not recognized!")
        exit
    
    
    