#!/usr/bin/python3
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import hashlib, subprocess, re, os
import argparse
import xml.etree.ElementTree as ET
import xml.dom.minidom as mdom
import otbApplication



def write_xml_results(file, tree):
    xml_string = ET.tostring(tree).decode()
    parsed_xml = mdom.parseString(xml_string)
    pretty_xml_as_string = parsed_xml.toprettyxml()

    file.write(pretty_xml_as_string)
    

if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Download, md5 check and unzip Sentinel from Copernicus cart file in current directory.
        """)

    parser.add_argument('-carttype', action='store', required=True, help='CVS=1 or XML=2 SciHub Copernicus Cart File type')
    parser.add_argument('-cartfile', action='store', required=True, help='SciHub Copernicus Cart File (.csv or .meta4)')
    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    
    
    args=parser.parse_args()
    
    print("reading file : "+args.cartfile)
    
    file = open(args.cartfile, 'r')
    xmlstring = file.read()
    file.close()
    results = open("./download_results.xml", 'w')

    xml_string = re.sub(' xmlns="[^"]+"', '', xmlstring, count=1)
    
    tree = ET.fromstring(xml_string)
#    root = tree.getroot()
    
#    ns = {'b':"urn:ietf:params:xml:ns:metalink"}
    
    for file in tree.findall('file'): #b:,ns
        
        name = file.get('name')
        status = ET.SubElement(file, 'download')
        if not os.path.isfile(name):
            url = file.find('url').text
            spilturl = url.split('$')
            spilturl[0] = spilturl[0]+"\\"
            url = '$'.join(spilturl)
            
            status.text = "starting"
            print("start downloading file : "+name)

            cmd='wget --no-check-certificate --user='+args.user+' --password='+args.psswd+'  --tries=5 --progress=dot -e dotbytes=10M -c -O '+name+' "'+ url +'"'
            print("command : "+cmd)
            p = subprocess.Popen(cmd, shell=True)
            p.wait() 
            status.text = "ended"
            write_xml_results(results, tree)
            
        
        status.text = "checking"
        print("check downloaded file : "+name)
        # Correct original md5 goes here
        original_md5 = file.find('hash').text    #b:,ns

        # Open,close, read file and calculate MD5 on its contents 
        with open(name) as file_to_check:
            # read contents of the file
            data = file_to_check.read()    
            # pipe contents of the file through
            md5_returned = hashlib.md5(data).hexdigest().upper()

        print("O = "+original_md5)
        print("R = "+md5_returned)
        # Finally compare original MD5 with freshly calculated
        if original_md5 == md5_returned:
            print("MD5 verified.")
            status.text = "checked"
        else:
            print("MD5 verification failed!.")
            status.text = "failed"
        
        write_xml_results(results, tree)
        
        if status.text == "checked":
            cmd='unzip '+name
            print("command : "+cmd)
            p = subprocess.Popen(cmd, shell=True)
            p.wait() 
            status.text = "unzipped"
            write_xml_results(results, tree)
    
    
#    for file in tree.findall('file'):
#        name = file.get('name')
#        dataset = name.replace('.zip', '.SAFE')
#        if not os.path.isdir(dataset) :
#            continue
#        
#        if  dataset.startswith('S2') and dataset.find('L2A'):
#            'S2A_MSIL2A_20180413T095031_N0207_R079_T33UXP_20180413T115829.SAFE/GRANULE/L2A_T33UXP_A014662_20180413T095634/IMG_DATA/R10m/T33UXP_20180413T095031_B04_10m.jp2'
            
        
    
    results.close()
    #tree.write('output.xml')
    print("Done!")
        
