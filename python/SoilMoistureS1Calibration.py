#!/usr/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import os
import xml.etree.ElementTree as ET
import argparse
import otbApplication, math
from osgeo import gdal, osr, ogr
from subprocess import Popen, PIPE

globconfig = {
    "urlsrmt":"http://step.esa.int/auxdata/dem/SRTMGL1/",
    "srtmhgtzip":"/media/sf_D_DRIVE/partage_vbox/Donnees/SRTMHGTZIP",
    "demtif":"/media/sf_D_DRIVE/partage_vbox/Donnees/SRTMHGTZIP/TIF",
    "geoid":"/media/sf_D_DRIVE/partage_vbox/Donnees/egm96.grd"
    }
namespaces = {
    'gml': "http://www.opengis.net/gml"
    }
overwrite=False

def search_files(directory='.', resolution='S1', extension='SAFE', fictype='d'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit()
            
    return images

def process_command(cmd):
    print("Starting : "+" ".join(cmd))
    p = Popen(cmd, stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("process failed %d : %s" % (p.returncode, output))
    print("#################################################")
    return p.returncode

def get_img_extend(img):
    raster = gdal.Open(img)
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    
    upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
    cols = raster.RasterXSize
    rows = raster.RasterYSize
     
    ulx = upx + 0*xres + 0*xskew
    uly = upy + 0*yskew + 0*yres
     
    llx = upx + 0*xres + rows*xskew
    lly = upy + 0*yskew + rows*yres
     
    lrx = upx + cols*xres + rows*xskew
    lry = upy + cols*yskew + rows*yres
     
    urx = upx + cols*xres + 0*xskew
    ury = upy + cols*yskew + 0*yres
    
    pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointLL.AddPoint(llx, lly)
    pointUR.AddPoint(urx, ury)
    
    if not proj.IsGeographic() :
        
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(4326)
        coordTransform = osr.CoordinateTransformation(proj, outSpatialRef)
        
        pointLL.Transform(coordTransform)
        pointUR.Transform(coordTransform)
        
    return pointLL, pointUR, cols, rows

def find_srtm_hgt_name(llx, lly , urx, ury ):
    
    
    pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
    pointLL.AddPoint(llx, lly)
    pointUR.AddPoint(urx, ury)
    
    tilesname=[]
    print("LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
    
    for o in range(math.ceil(pointUR.GetX()) - math.floor(pointLL.GetX())):
        for a in range(math.ceil(pointUR.GetY()) - math.floor(pointLL.GetY())):
            lat =  math.floor(pointLL.GetY()) + a
            lon =  math.floor(pointLL.GetX()) + o
            if lat >= 0 :
                hem = 'N'
            else : 
                hem = 'S'
                lat = abs(lat)
            if lon >= 0 :
                grw = 'E'
            else : 
                grw = 'W'
                lon = abs(lon)
            tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
            
    return tilesname

def prepare_srtm_hgt(sarsafe):
    #get tiles
    tree = ET.parse(os.path.join(sarsafe,"manifest.safe"))
    root = tree.getroot()
    
    footprintnode = root.find(".//gml:coordinates",namespaces)
    footprintcoordsp = footprintnode.text.split(" ")
    footprintlats = []
    footprintlons = []
    for coord in footprintcoordsp:
        coordsp = coord.split(",")
        footprintlats.append(float(coordsp[0]))
        footprintlons.append(float(coordsp[1]))
        
    llx, lly = min(footprintlons),min(footprintlats)
    urx, ury = max(footprintlons),max(footprintlats)
    
    tilesname = find_srtm_hgt_name(llx, lly , urx, ury )
    
    print("Checking SRTM tiles: "+str(tilesname))
    tilefiles = []
    if len(tilesname) == 0:
        print("bug")
        exit()
    
    srtmsuf = ".SRTMGL1.hgt.zip"   
    
    tiftiles=[]
    for tile in tilesname :
        tilef = os.path.join(globconfig["srtmhgtzip"],tile + srtmsuf)
        if not os.path.exists(tilef):
            print("Download SRTM tile : "+tilef)
            cmd=["wget","-P", globconfig['srtmhgtzip'],globconfig["urlsrmt"]+tile + srtmsuf]
            rcode = process_command(cmd)
            
            if rcode != 0:
                continue
        
        hgtdir = os.path.join(globconfig["srtmhgtzip"],"HGT")
        tifdir = os.path.join(globconfig["srtmhgtzip"],"TIF")
        tilehgt = os.path.join(hgtdir,tile+".hgt")
        tiletif = os.path.join(tifdir,tile+".tif")
        
        if not os.path.exists(tilehgt):
            print("Unzip SRTM tile : "+tilehgt)
            cmd=['unzip','-d',hgtdir,tilef]   
            process_command(cmd)
        
        if not os.path.exists(tiletif):
            print("Convert SRTM tile : "+tiletif)
            cmd=['gdal_translate',tilehgt,tiletif]   
            process_command(cmd)
        
        tiftiles.append(tiletif)
    
    print("Ok for SRTM tiles: "+str(tilesname))
    return tiftiles

def create_zone_dem_aspect_and_slope(ref,zone,outdir):
    
    outdem = os.path.join(outdir,"SRTM_"+zone+".TIF")
    if os.path.exists(outdem) and not overwrite :
        return outdem
        
    #get tiles
    pointLL, pointUR, cols, rows = get_img_extend(ref)
    
    tilesname = find_srtm_hgt_name(pointLL.GetX(), pointLL.GetY() , pointUR.GetX(), pointUR.GetY() )
    
    tilefiles = []
    for tile in tilesname:
        tifdir = os.path.join(globconfig["srtmhgtzip"],"TIF")
        tilefiles.append(os.path.join(tifdir,tile+".tif"))
    
    print("Generating DEM file on ref:"+ref)
    
    mosapp = otbApplication.Registry.CreateApplication("Mosaic")
    mosparams = {"il":tilefiles, "out":"mosatemp.tif"}
    mosapp.SetParameters(mosparams)
    mosapp.Execute()
    
    supapp = otbApplication.Registry.CreateApplication("Superimpose")
    supapp.SetParameterString("inr", ref)
    supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
    supapp.SetParameterString("out", outdem)
    supapp.ExecuteAndWriteOutput()
    
    #Compute slope & aspect
    print("Generating slope file...")
    outslope = os.path.join(outdir,"SLOPE_"+zone+".TIF")
    cmd=['gdaldem', 'slope', '-p', outdem, outslope]
    
    process_command(cmd)
    
    print("Generating aspect file...")
    outaspect = os.path.join(outdir,"ASPECT_"+zone+".TIF")
    cmd=['gdaldem', 'aspect', outdem, outaspect]
    
    process_command(cmd)
    
    return outdem
        
def create_incidenceAngle_from_surface_raster(annofiles,dem, img):
    
    outdir = os.path.dirname(img)
    tempdir = os.path.join(os.path.dirname(img),"auxfiles")
    imgbs = os.path.basename(img)
    imgsp = os.path.basename(img).split("_")
    #todo: mettre un dosiier temp
    outinc = os.path.join(tempdir,"_".join(imgsp[:-1])+"_THETASURF.TIF")
    outincfromdem = os.path.join(outdir,"_".join(imgsp[:-1])+"_THETA.TIF")  
    vrtfile = os.path.join(tempdir,"_".join(imgsp[:-1])+"_THETA.vrt")
    csvfile = os.path.join(tempdir,"_".join(imgsp[:-1])+"_THETA.csv")
    layername = "_".join(imgsp[:-1])+"_THETA"
    
    if os.path.exists(outinc) and not overwrite :
        return outinc
    
    if not os.path.exists(tempdir):
        os.mkdir(tempdir)
    
    pointLL, pointUR, cols, rows = get_img_extend(img)
    
    if not os.path.exists(csvfile):
        for annofile in annofiles:
    
            tree = ET.parse(annofile)
            root = tree.getroot()
            
            geoloclist = root.find(".//geolocationGridPointList")
            
            if geoloclist == None :
                print("Can't find geolocationGridPointList tag in xml file.")
                exit()
            
            header = True
            if os.path.exists(csvfile):
                header = False
            if not os.path.exists(vrtfile):
                with open(vrtfile,'w') as vrt:
                    vrt.write("<OGRVRTDataSource>\n")
                    vrt.write('    <OGRVRTLayer name="'+layername+'">\n')
                    vrt.write('        <SrcDataSource>'+csvfile+'</SrcDataSource>\n')
                    vrt.write("        <GeometryType>wkbPoint</GeometryType>\n")
                    vrt.write("        <LayerSRS>WGS84</LayerSRS>\n")
                    vrt.write('        <GeometryField encoding="PointFromColumns" x="Easting" y="Northing" z="incidence"/>\n')
                    vrt.write("    </OGRVRTLayer>\n")
                    vrt.write("</OGRVRTDataSource>\n")
                
            with open(csvfile,'a') as csvf:
                if header:
                    csvf.write("Easting,Northing,incidence\n")
                for child in geoloclist:
                    lat = child.find(".//latitude").text
                    lon = child.find(".//longitude").text
                    alti = float(child.find(".//height").text)
                    incdem = float(child.find(".//incidenceAngle").text)
                    inc = incdem - alti/693000.
                    csvf.write(lon+","+lat+","+str(incdem)+"\n")
     
    
    cmd = ["gdal_grid","-a","linear","-txe", str(pointLL.GetX()), str(pointUR.GetX()), "-tye", str(pointLL.GetY()), str(pointUR.GetY()), "-outsize", str(cols), str(rows), "-of", "GTiff", "-ot", "Float32", "-l", layername, vrtfile, outinc]
    
    process_command(cmd)
    
    
    return outinc

def create_incidenceAngle_from_ellipsoid_raster(vvortho,dem,thetasurf):
    
    outdir = os.path.dirname(thetasurf)
    
    outthetaelli = thetasurf.replace("_THETASURF","_THETAELLI")  
    
    
    if os.path.exists(outthetaelli) and not overwrite :
        return outthetaelli
    
    
    app41 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app41.SetParameterString("inm", thetasurf)
    app41.SetParameterString("inr", vvortho)
    app41.SetParameterString("interpolator","bco")
    app41.SetParameterString("out", "temp41.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app41.Execute()
    print("End of Resampling \n")
    
    app42 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app42.SetParameterString("inm", dem)
    app42.SetParameterString("inr", vvortho)
    app42.SetParameterString("interpolator","bco")
    app42.SetParameterString("out", "temp41.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app42.Execute()
    print("End of Resampling \n")
    
    appS = otbApplication.Registry.CreateApplication("BandMath")
    appS.SetParameterStringList("il", [vvortho])
    appS.AddImageToParameterInputImageList("il", app42.GetParameterOutputImage("out"))
    appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out"))
    # Define Input im2: Band Red (B4)
    appS.SetParameterString("out", "tempS.tif")
    appS.SetParameterString("exp", "im1b1 == 0?0:im3b1 - im2b1/693000" )
    
    appS.Execute()
    
    appM = otbApplication.Registry.CreateApplication("ManageNoData")
    appM.SetParameterInputImage("in", appS.GetParameterOutputImage("out"))
    appM.SetParameterString("out", outthetaelli+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    appM.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    appM.SetParameterString("mode", "changevalue")
    appM.ExecuteAndWriteOutput()
    
    return outthetaelli

def create_local_incidenceAngle_raster(annofiles, img, dem, thetasurf):
    
    outdir = os.path.dirname(img)
    
    inslope = dem.replace("SRTM_", "SLOPE_")
    inaspect = dem.replace("SRTM_", "ASPECT_")
    #todo: mettre un dosiier temp
    outthetaloc = thetasurf.replace("_THETASURF","_THETALOC")  
    
    if os.path.exists(outthetaloc) and not overwrite :
        return outthetaloc
    
    
    plateformheading=[]
    for annofile in annofiles:
    
        tree = ET.parse(annofile)
        root = tree.getroot()
        
        plateformheading.append(float(root.find(".//platformHeading").text))
        
    if len(plateformheading) == 0:
        print("Error: Can't find plateformHeading in annotation files")
        exit()
    elif len(plateformheading) == 1:
        azimuthsat = (plateformheading[0]+90) % 360
    else:
        azimuthsat = (math.fsum(plateformheading)/len(plateformheading)+90) % 360
        

    
    app41 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app41.SetParameterString("inm", thetasurf)
    app41.SetParameterString("inr", img)
    app41.SetParameterString("interpolator","bco")
    app41.SetParameterString("out", "temp41.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app41.Execute()
    print("End of Resampling \n")
    
    app43 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app43.SetParameterString("inm", inslope)
    app43.SetParameterString("inr", img)
    app43.SetParameterString("interpolator","bco")
    app43.SetParameterString("out", "temp43.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app43.Execute()
    print("End of Resampling \n")
    
    app44 = otbApplication.Registry.CreateApplication("Superimpose")
    # The following lines set all the application parameters:
    app44.SetParameterString("inm", inaspect)
    app44.SetParameterString("inr", img)
    app44.SetParameterString("interpolator","bco")
    app44.SetParameterString("out", "temp44.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app44.Execute()
    print("End of Resampling \n")
    
    appS = otbApplication.Registry.CreateApplication("BandMath")
    appS.SetParameterStringList("il", [img])  #im1b1 -> vv
    appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out")) #im2b1 -> incidence surf
    appS.AddImageToParameterInputImageList("il", app43.GetParameterOutputImage("out")) #im3b1 -> slope
    appS.AddImageToParameterInputImageList("il", app44.GetParameterOutputImage("out")) #im4b1 -> aspect
    # Define Input im2: Band Red (B4)
    appS.SetParameterString("out", "tempS.tif")
    appS.SetParameterString("exp", "im1b1 == 0?0:acos(cos(im3b1*"+str(math.pi/400)+")*cos(im2b1*"+str(math.pi/180)+")-sin(im3b1*"+str(math.pi/400)+")*sin(im2b1*"+str(math.pi/180)+")*cos(("+str(azimuthsat)+"-im4b1)*"+str(math.pi/180)+"))*"+str(180/math.pi)+"")
    
    appS.Execute()
    
    appM = otbApplication.Registry.CreateApplication("ManageNoData")
    appM.SetParameterInputImage("in", appS.GetParameterOutputImage("out"))
    appM.SetParameterString("out", outthetaloc+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
    appM.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
    appM.SetParameterString("mode", "changevalue")
    appM.ExecuteAndWriteOutput()
    
    return outthetaloc

    
    
def cal_ortho_sub_mosa_pipeline(sarfiles, ref, outfile):
    
    print("processing.......................................................")
#     print("image1 = "+mos1)
#     print("image2 = "+mos2)
#     print("output = "+out)
    apps=[]
    i=0
    for sarfile in sarfiles:
        apps.append(otbApplication.Registry.CreateApplication("SARCalibration"))
        apps[i].SetParameterString("in",sarfile)
        apps[i].SetParameterString("out", str(i)+"temp.tif")
        apps[i].Execute()
        
        apps.append(otbApplication.Registry.CreateApplication("BandMath"))
        apps[i+1].AddImageToParameterInputImageList("il",apps[i].GetParameterOutputImage("out"))
        apps[i+1].SetParameterString("out", str(i+1)+"temp.tif")
        apps[i+1].SetParameterString("exp", "im1b1<=0?0:im1b1")
        apps[i+1].Execute()
    
        apps.append(otbApplication.Registry.CreateApplication("OrthoRectification"))
        apps[i+2].SetParameterInputImage("io.in",apps[i+1].GetParameterOutputImage("out"))
        apps[i+2].SetParameterString("io.out",str(i+2)+"temp.tif")
        apps[i+2].SetParameterFloat("opt.gridspacing",40)
        apps[i+2].SetParameterString("elev.dem", globconfig["demtif"])
        apps[i+2].SetParameterString("elev.geoid", globconfig["geoid"])
        apps[i+2].Execute()
    
        apps.append(otbApplication.Registry.CreateApplication("Superimpose"))
        apps[i+3].SetParameterInputImage("inm",apps[i+2].GetParameterOutputImage("io.out"))
        apps[i+3].SetParameterString("out", str(i+3)+"temp.tif")
        apps[i+3].SetParameterString("interpolator","nn")
        apps[i+3].SetParameterString("inr",ref)
        apps[i+3].Execute()
        
        i+=4
        
    if i == 8 :
        apps.append(otbApplication.Registry.CreateApplication("Mosaic"))
        apps[i].AddImageToParameterInputImageList("il",apps[3].GetParameterOutputImage("out"))
        apps[i].AddImageToParameterInputImageList("il", apps[7].GetParameterOutputImage("out"))
        apps[i].SetParameterString("out",  str(i)+"temp.tif")
        apps[i].Execute()
        
        apps.append(otbApplication.Registry.CreateApplication("ManageNoData"))
        apps[i+1].SetParameterInputImage("in", apps[8].GetParameterOutputImage("out"))
        apps[i+1].SetParameterString("out", outfile+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        apps[i+1].SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
        apps[i+1].SetParameterString("mode", "changevalue")
        apps[i+1].ExecuteAndWriteOutput()
    else:
        apps.append(otbApplication.Registry.CreateApplication("ManageNoData"))
        apps[i].SetParameterInputImage("in", apps[3].GetParameterOutputImage("out"))
        apps[i].SetParameterString("out", outfile+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
        apps[i].SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_float)
        apps[i].SetParameterString("mode", "changevalue")
        apps[i].ExecuteAndWriteOutput()
        
        
            
    print("done.............................................................")
    
if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        S1 Calibration, orhorectification, dem, slope, aspect and incidence angle processing
        """)
    
    parser.add_argument('-indir', action='store', required=True, help='Directory containing Sentinel1 SAFE dir')
    parser.add_argument('-inref', action='store', required=True, help='Image reference, generally corresponding to a S2 tile')
    parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    parser.add_argument('-theta', choices=['loc', 'elli'],  default='loc', required=False, help='[Optional]Create local incidence raster or incidence raster from ellipsoid, default loc')
    parser.add_argument('--overwrite', dest='overwrite', action='store_true',required=False, help='[Optional] Overwrite already existing files (default False)')
    parser.set_defaults(overwrite=False)
    
    
    args=parser.parse_args()
    
    overwrite = args.overwrite
    
    raster = gdal.Open(args.inref)
    proj = osr.SpatialReference(wkt=raster.GetProjection())
    if proj.IsGeographic() :
        print("Error: Reference image must be in projected coordinates in order to create slope")
        exit()
    
    if not os.path.exists(args.outdir):
        os.mkdir(args.outdir)
        
    demdir = os.path.join(args.outdir,"DEM")
    if not os.path.exists(demdir):
        os.mkdir(demdir)
    
    indir=[]
    indir=search_files(args.indir, 'S1', 'SAFE', 'd')
    
    
    zonedemfile = create_zone_dem_aspect_and_slope(args.inref,args.zone,demdir)
    
    dejafait=[]
    for file in indir:
        if file in dejafait:
            continue
        
        prepare_srtm_hgt(file)
        
        bname = os.path.basename(file).split(".")[0]
        splitbname = bname.split("_")
        plateforme = splitbname[0]
        acqdate = splitbname[4].split("T")[0]  
        acqtime = splitbname[4].split("T")[1]
        
        
        outfilebase = os.path.join(args.outdir,"_".join(splitbname[:3])+"_"+args.zone.upper()+"_"+acqdate+"T"+acqtime)
       
        l = [indir.index(i) for i in indir if (acqdate in i) and (plateforme in i)]
        
        
        if len(l) == 2 :
            in1vv = search_files(indir[l[0]], '-vv-', 'tiff', 'f')
            in2vv = search_files(indir[l[1]], '-vv-', 'tiff', 'f')
            
            if len(in1vv) !=1 or len(in2vv) !=1:
                print("Error: Can't find tiff files!")
                continue
            sarfiles = [in1vv[0],in2vv[0]]
            outfile = outfilebase +"_VV.TIF"
            
            dejafait.append(indir[l[0]])
            dejafait.append(indir[l[1]])
            
            if not os.path.exists(outfile) or overwrite :        
                cal_ortho_sub_mosa_pipeline(sarfiles, args.inref, outfile)
            
            
            
            anno1vv = search_files(indir[l[0]], '-vv-', 'xml', 'f')
            anno2vv = search_files(indir[l[1]], '-vv-', 'xml', 'f')
            annofiles=[]
            for a in anno1vv:
                if a.find("calibration") >= 0 or a.find("noise") >= 0:
                    continue
                annofiles.append(a)
            for a in anno2vv:
                if a.find("calibration") >= 0 or a.find("noise") >= 0:
                    continue
                annofiles.append(a) 
                
            if len(annofiles) != 2:
                print("Error : Can't find annotation files")
                exit()
            
        elif len(l) == 1 :
            in1vv = search_files(file, '-vv-', 'tiff', 'f')
            
            if len(in1vv) !=1:
                print("Error: Can't find tiff files!")
                continue
            
            outfile = outfilebase +"_VV.TIF"
            
            if not os.path.exists(outfile) or overwrite :         
                cal_ortho_sub_mosa_pipeline(in1vv, args.inref, outfile)
            
            
            
            anno1vv = search_files(file, '-vv-', 'xml', 'f')
            annofiles=[]
            for a in anno1vv:
                if a.find("calibration") >= 0 or a.find("noise") >= 0:
                    continue
                annofiles.append(a)
                
            if len(annofiles) != 1:
                print("Error : Can't find annotation files")
                exit()
            
        else :
            print("Error on index :"+l)
    
        thetasurf = create_incidenceAngle_from_surface_raster(annofiles,zonedemfile, outfile)
        if args.theta == "elli":
            create_incidenceAngle_from_ellipsoid_raster(outfile,zonedemfile,thetasurf)
        elif args.theta == "loc":
            create_local_incidenceAngle_raster(annofiles, outfile, zonedemfile, thetasurf)
        
    
