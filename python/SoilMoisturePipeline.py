#!/usr/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

__author__ = "Loic Lozach"
__date__ = "$Dec 14, 2018 12:40:21 PM$"

import os, argparse, subprocess, math, datetime,glob, re
import otbApplication
from osgeo import gdal, osr, ogr
from subprocess import Popen, PIPE

maskedlabels, labels, vectorlabels, labelsfield, lulc, agrivalues, ndvi, sarvv, \
    slope, slopevalue, sarth, modeldir, modelchoice, output, outformat, sarmode = "","","","","","","","","","","","","","","",""

def search_files(directory='.', resolution='NDVI', extension='tif', fictype='f'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    for dirpath, dirnames, files in os.walk(directory):
        if fictype == 'f':
            for name in files:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, name))
                    images.append(abspath)
        elif fictype == 'd':
            for dirname in dirnames:
    #            print(os.path.join(dirpath, name) + " test")
                if extension and dirname.lower().endswith(extension) and dirname.lower().find(resolution) >= 0 :
                    
    #                print(os.path.join(dirpath, name) + " OK")
                    abspath = os.path.abspath(os.path.join(dirpath, dirname))
                    images.append(abspath)
        else:
            print("search_files type error")
            exit()
            
    return images

    
def reprojectRaster(absfile, indeximg):
    global sarvv, ndvi, maskedlabels, slope
    
    ext=absfile.split(".")[1]
    longname=absfile.split(".")[0]
    if ext.lower() == "img" :
        temp_file = absfile
        absfile = longname+".tif"
    else :
        temp_file = longname + ".old." + ext
        os.rename(absfile, temp_file)
    
    
    p = Popen(['gdalwarp', '-t_srs', 'EPSG:4326', temp_file, absfile], stdout=PIPE)
#    p.wait()
    output = p.communicate()[0]
    if p.returncode != 0: 
        print("gdalwarp failed %d : %s" % (p.returncode, output))
#        with open("gdalwrap_log.err",'a') as err:
#            err.write("######################################################################################################\n")
#            err.write(absfile)
#            err.write(output)
#            err.write("\n######################################################################################################")
        if ext.lower() == "img" :
            os.remove(absfile)
        else :
            os.remove(absfile)
            os.rename(temp_file, absfile)
        return None, 1
    
    if ext.lower() == "img" :
        if indeximg == 0 :
            sarvv = absfile
        elif indeximg == 1 :
            ndvi = absfile
        elif indeximg == 2 :
            maskedlabels = absfile
        elif indeximg == 3 :
            slope = absfile
        
    print("gdalwarp succeeded on : "+absfile)
    os.remove(temp_file)
    return absfile, 0

def normalize_proj_and_extend():
    
    
    if slope == None :
        images = [sarvv, ndvi, maskedlabels]
    else:
        images = [sarvv, ndvi, maskedlabels, slope]
    extends = []
    i=0
    for img in images:
        print("Normalizing "+img)
        raster = gdal.Open(img)
        proj = osr.SpatialReference(wkt=raster.GetProjection())
        
        if not (proj.IsGeographic() and proj.GetAttrValue('GEOGCS').find("WGS") >= 0 and proj.GetAttrValue('GEOGCS').find("84") >= 0) :
            
            print("Reprojection needed. Launching gdalwrap on "+img)
            img, projerr = reprojectRaster(img,i)
            if projerr:
                print("Error on reprojection to WGS84")
                return 1
        
            del raster
            raster = gdal.Open(img)

        upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
        cols = raster.RasterXSize
        rows = raster.RasterYSize
         
        ulx = upx + 0*xres + 0*xskew
        uly = upy + 0*yskew + 0*yres
         
        llx = upx + 0*xres + rows*xskew
        lly = upy + 0*yskew + rows*yres
         
        lrx = upx + cols*xres + rows*xskew
        lry = upy + cols*yskew + rows*yres
         
        urx = upx + cols*xres + 0*xskew
        ury = upy + cols*yskew + 0*yres
        
        extends.append("POLYGON (("+str(ulx)+" "+str(uly)+", "+str(llx)+" "+str(lly)+", "+str(lrx)+" "+str(lry)+", "+str(urx)+" "+str(ury)+", "+str(ulx)+" "+str(uly)+"))")
        
        raster = None
        i+=1
#    print(extends)
    print("Finding images intersection")
    intersection = None
    for i in range(len(images)-1):
        if i == 0:
            poly1 = ogr.CreateGeometryFromWkt(extends[i])
        else:
            poly1 = intersection
        poly2 = ogr.CreateGeometryFromWkt(extends[i+1])
        
        intersection = poly1.Intersection(poly2)
        
    # Get Envelope returns a tuple (minX, maxX, minY, maxY)
    intersectionEnv = intersection.GetEnvelope()
    print("Images will be subsat at :"+str(intersectionEnv))
#    print("minX: %d, minY: %d, maxX: %d, maxY: %d" %(intersectionEnv[0],intersectionEnv[2],intersectionEnv[1],intersectionEnv[3]))
    
    
    return intersectionEnv

def get_meteodates_from_sardate(sardate):
    exformat = "%Y%m%d"
    saryear,sarmonth,sarday = sardate[:4],sardate[4:6],sardate[6:8]
    fsardate = datetime.date(int(saryear),int(sarmonth),int(sarday))
    oneday = datetime.timedelta(days=1)
    sardatem1 = (fsardate - oneday).strftime(exformat)
    sardatem2 = (fsardate - oneday - oneday).strftime(exformat)
    return [sardate,sardatem1,sardatem2]


def download_gpm(sarlist, sartype, gpmdir):
    datepos=None
    if sartype == "snap":
        print("#date position")
        datepos=4
    elif sartype == "s2tile":
        datepos=4
    else:
        print("Error on sartype!")
        exit()
    
    if not os.path.exists(gpmdir):
        os.mkdir(gpmdir)
        
    cmd = "wget --user=loic.lozach@irstea.fr --password=loic.lozach@irstea.fr \
            ftp://arthurhou.pps.eosdis.nasa.gov/gpmdata/"
    arg0 = "wget"
    arg1 = "--user=loic.lozach@irstea.fr"
    arg2 = "--password=loic.lozach@irstea.fr"
    arg3 = "ftp://arthurhou.pps.eosdis.nasa.gov/gpmdata/"
    
    for sar in sarlist:
        sardata = os.path.basename(sar)
        sarsplit = sardata.split('_')
        sardate = sarsplit[datepos][:8]
        sar3dates = get_meteodates_from_sardate(sardate)
        
        tifexits = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"-S000000-E235959*.tif"))
        if len(tifexits) >= 1 :
            continue
        
        tifexitsm1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"-S000000-E235959*.tif"))
        if len(tifexitsm1) >= 1 :
            continue
        
        tifexitsm2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"-S000000-E235959*.tif"))
        if len(tifexitsm2) >= 1 :
            continue
        
        
        gpmtif = sar3dates[0][:4] +"/"+ sar3dates[0][4:6] +"/"+ sar3dates[0][6:8] +"/gis/"+ "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"-S000000-E235959*.t*"
        gpmtifm1 = sar3dates[1][:4] +"/"+ sar3dates[1][4:6] +"/"+ sar3dates[1][6:8] +"/gis/"+  "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"-S000000-E235959*.t*"
        gpmtifm2 = sar3dates[2][:4] +"/"+ sar3dates[2][4:6] +"/"+ sar3dates[2][6:8] +"/gis/"+  "3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"-S000000-E235959*.t*"
        cmdlist=[]
        cmdlist.append([arg0,arg1,arg2,arg3 + gpmtif])
        cmdlist.append([arg0,arg1,arg2,arg3 + gpmtifm1])
        cmdlist.append([arg0,arg1,arg2,arg3 + gpmtifm2])
        
        for c in cmdlist:
            p = Popen(c, cwd=gpmdir, stdout=PIPE)
            output = p.communicate()[0]
            if p.returncode != 0: 
                print("wget failed %d : %s" % (p.returncode, output))
                
def process_gpm(sar, sar3dates, gpmdir):
    outdir = os.path.join( os.path.dirname(gpmdir),"MaskWetOrDry")
    outfile = os.path.join(outdir,"MaskWetOrDry"+args.zone+"_"+sar3dates[0]+".tif")
    if os.path.exists(outfile):
        print("Already exists : "+outfile)
        return
    if not os.path.exists(outdir):
        os.mkdir(outdir)
        
    gpmfiles1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif"))
    gpmfiles2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif"))
    gpmfiles3 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif"))
    
    if  len(gpmfiles1) == 0 :
        print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
        return 1
    if  len(gpmfiles2) == 0 :
        print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
        return 1
    if  len(gpmfiles3) == 0 :
        print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
        return 1
    
    gpmfiles=[gpmfiles1[0],gpmfiles2[0],gpmfiles3[0]]
    apps=[]
    for f in gpmfiles :
        
        app = otbApplication.Registry.CreateApplication("Superimpose")
        
        app.SetParameterString("inm", f)
        app.SetParameterString("inr", sar)
        app.SetParameterString("interpolator","bco")
        app.SetParameterString("out", "temp2.tif")
        
        app.Execute() #ExecuteAndWriteOutput() 
        apps.append(app)
    
    app0 = otbApplication.Registry.CreateApplication("BandMath")
    app0.AddImageToParameterInputImageList("il", apps[0].GetParameterOutputImage("out"))
    app0.AddImageToParameterInputImageList("il", apps[1].GetParameterOutputImage("out"))
    app0.AddImageToParameterInputImageList("il", apps[2].GetParameterOutputImage("out"))
    app0.SetParameterString("out", outfile)
    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    app0.SetParameterString("exp", "im1b1+im2b1+im3b1>5?2:1")
    app0.ExecuteAndWriteOutput()
    
    
def process_gpm_noresampling(sar, sar3dates, gpmdir):
    outdir = os.path.join( os.path.dirname(gpmdir),"CumulPluie")
    outfile = os.path.join(outdir,"CumulPluie_"+args.zone+"_"+sar3dates[0]+".tif")
    if os.path.exists(outfile):
        print("Already exists : "+outfile)
        return
    
    if not os.path.exists(outdir):
        os.mkdir(outdir)
        
    gpmfiles1 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif"))
    gpmfiles2 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif"))
    gpmfiles3 = glob.glob(os.path.join(gpmdir,"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif"))
    
    if  len(gpmfiles1) == 0 :
        print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[0]+"*.tif")
        return 1
    if  len(gpmfiles2) == 0 :
        print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[1]+"*.tif")
        return 1
    if  len(gpmfiles3) == 0 :
        print("Error: Can't find gpm file "+"3B-DAY-GIS.MS.MRG.3IMERG."+sar3dates[2]+"*.tif")
        return 1
    
    gpmfiles=[gpmfiles1[0],gpmfiles2[0],gpmfiles3[0]]
    apps=[]
    for f in gpmfiles :
        
        app = otbApplication.Registry.CreateApplication("ExtractROI")
        
        app.SetParameterString("in", f)
        app.SetParameterString("mode.fit.im", sar)
        app.SetParameterString("mode","fit")
        app.SetParameterString("out", "temp2.tif")
        
        app.Execute() #ExecuteAndWriteOutput() 
        apps.append(app)
    
    app0 = otbApplication.Registry.CreateApplication("BandMath")
    app0.AddImageToParameterInputImageList("il", apps[0].GetParameterOutputImage("out"))
    app0.AddImageToParameterInputImageList("il", apps[1].GetParameterOutputImage("out"))
    app0.AddImageToParameterInputImageList("il", apps[2].GetParameterOutputImage("out"))
    app0.SetParameterString("out", outfile)
    app0.SetParameterString("exp", "im1b1+im2b1+im3b1")
    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint16)
    app0.ExecuteAndWriteOutput()
    
#     app1 = otbApplication.Registry.CreateApplication("Smoothing")
#     app1.SetParameterInputImage("in", app0.GetParameterOutputImage("out"))
#     app1.SetParameterString("type", "gaussian")
#     app1.SetParameterString("out", outfile)
#     app1.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint16)
#     app1.ExecuteAndWriteOutput()
    
def select_tfmodel_from_gpm(sardate,gpmdir):
    cumul = glob.glob(os.path.join( os.path.join( os.path.dirname(gpmdir),"CumulPluie"), "CumulPluie_"+args.zone+"_"+sardate+".tif"))
    
    if len(cumul) != 1 :
        print("Error : Can't find file "+"CumulPluie_"+args.zone+"_"+sardate+".tif")
        exit()
        
    raster = gdal.Open(cumul[0])
    srcband = raster.GetRasterBand(1)
    stats = gdal.Band.GetStatistics(srcband,1,1)
    
    if stats[2] > 5 :
        return str(stats[2]),"wet"
    else :
        return str(stats[2]),"dry"    
    
def wet_or_dry_pipeline(args):
    sars=[]
    datepos=None
    if args.sardirtype == "snap":
        sars=search_files(args.sardir, 'S1', 'data', 'd')
        datepos=4
    elif args.sardirtype == "s2tile":
        datepos=4
        if args.sarmode == "vv":
            sars=search_files(args.sardir, 'S1', 'VV.TIF', 'f')
        elif args.sarmode == "vh":
            sars=search_files(args.sardir, 'S1', 'VH.TIF', 'f')
        else:
            print("Error: Exception on sarmode")
            exit()
    else:
        print("ERROR: wrong sardirtype argument")
        exit()
    
    download_gpm(sars, args.sardirtype, args.gpmdir)
    affectation=[]
    with open(args.outtxt, 'w') as affout:
        for sar in sars :
            sardata = os.path.basename(sar)
            sarsplit = sardata.split('_')
            sardatetime = sarsplit[datepos]
            sardate = sardatetime.split("T")[0]
            sar3dates = get_meteodates_from_sardate(sardate)
            
            if args.resampling :
                process_gpm(sar, sar3dates, args.gpmdir)
            else:
                process_gpm_noresampling(sar, sar3dates, args.gpmdir)
            
            res=select_tfmodel_from_gpm(sardate, args.gpmdir)
            
            affout.write(sar+";"+res[0]+";"+res[1]+"\n")

def get_model4invertion():
    sardate = os.path.basename(sarvv).split("_")[4].split("T")[0]
    print("sardate:"+sardate)
    result=None
    with open(modelchoice) as mc:
        for line in mc:
            sline = line.split(";")
            dline = os.path.basename(sline[0]).split("_")[4].split("T")[0]
            if dline == sardate :
                result=sline[2][:-1]
                return result
    
    if result == None:
        print("Error : Can't find date in modelchoice file for sar :"+os.path.basename(sarvv))
        exit()
        
def csvtoraster_pipeline(csv,outmoist):
    
    app1 = otbApplication.Registry.CreateApplication("ApplyCsvLabelsFile")
    
    # The following lines set all the application parameters:
    app1.SetParameterString("incsv", csv)
    app1.SetParameterString("inlabels", maskedlabels)
    app1.SetParameterString("out", "temp1.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app1.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
    app6 = otbApplication.Registry.CreateApplication("BandMath")
    app6.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
    app6.SetParameterString("out", "temp2.tif")
    app6.SetParameterString("exp", "im1b1*5")
    
    print("Launching... BandMath(im1b1*5)")
    # The following line execute the application
    app6.Execute() #ExecuteAndWriteOutput() 
    print("End of BandMath \n")

#    Ajouter ManageNoData    
    app7 = otbApplication.Registry.CreateApplication("ManageNoData")
    app7.SetParameterInputImage("in", app6.GetParameterOutputImage("out"))
    app7.SetParameterString("out", outmoist+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")#
    app7.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    app7.SetParameterString("mode", "changevalue")
    
    
    print("Launching... ManageNoData")
    app7.ExecuteAndWriteOutput()
    print("Done \n")
    

def short_pipeline(intersectionEnv):
    global slopevalue
    
    #prepare temp files
    if intersectionEnv == 1:
        return 1
    
#    inref, inmove = choose_smallest4ref(sarvv, maskedlabels)
    
    # The following line creates an instance of the Superimpose application
    app1 = otbApplication.Registry.CreateApplication("ExtractROI")
    
    # The following lines set all the application parameters:
    app1.SetParameterString("in", sarvv)
    app1.SetParameterString("mode", "extent")
    app1.SetParameterFloat("mode.extent.ulx",intersectionEnv[0])
    app1.SetParameterFloat("mode.extent.uly",intersectionEnv[3])
    app1.SetParameterFloat("mode.extent.lrx",intersectionEnv[1])
    app1.SetParameterFloat("mode.extent.lry",intersectionEnv[2])
    app1.SetParameterString("mode.extent.unit","lonlat")
    app1.SetParameterString("out", "temp1.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app1.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
    app2 = otbApplication.Registry.CreateApplication("Superimpose")
    
    # The following lines set all the application parameters:
    app2.SetParameterString("inm", sarth)
    app2.SetParameterInputImage("inr", app1.GetParameterOutputImage("out"))
    app2.SetParameterString("interpolator","nn")
    app2.SetParameterString("out", "temp2.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app2.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
    app3 = otbApplication.Registry.CreateApplication("Superimpose")
    
    # The following lines set all the application parameters:
    app3.SetParameterString("inm", ndvi)
    app3.SetParameterInputImage("inr", app1.GetParameterOutputImage("out"))
    app3.SetParameterString("interpolator","nn")
    app3.SetParameterString("out", "temp3.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app3.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
    app4 = otbApplication.Registry.CreateApplication("Superimpose")
    
    # The following lines set all the application parameters:
    app4.SetParameterString("inm", maskedlabels)
    app4.SetParameterInputImage("inr", app1.GetParameterOutputImage("out"))
    app4.SetParameterString("interpolator","nn")
    app4.SetParameterString("out", "temp4.tif")
    
    print("Launching... Resampling")
    # The following line execute the application
    app4.Execute() #ExecuteAndWriteOutput() 
    print("End of Resampling \n")
    
       
    if not slope == None and len(slope) > 2:
        if slopevalue == None :
            slopevalue = str(20)
        
        app41 = otbApplication.Registry.CreateApplication("Superimpose")
    
        # The following lines set all the application parameters:
        app41.SetParameterString("inm", slope)
        app41.SetParameterInputImage("inr", app1.GetParameterOutputImage("out"))
        app41.SetParameterString("interpolator","nn")
        app41.SetParameterString("out", "temp41.tif")
        
        print("Launching... Resampling")
        # The following line execute the application
        app41.Execute() #ExecuteAndWriteOutput() 
        print("End of Resampling \n")
        
        appS = otbApplication.Registry.CreateApplication("BandMath")
        appS.AddImageToParameterInputImageList("il", app1.GetParameterOutputImage("out"))
        # Define Input im2: Band Red (B4)
        appS.AddImageToParameterInputImageList("il", app41.GetParameterOutputImage("out"))
        appS.SetParameterString("out", "tempS.tif")
        appS.SetParameterString("exp", "im2b1<"+slopevalue+"?im1b1:0")
        print("Filtering... Slope")
        appS.Execute()
        print("End of Slope Filtering \n")
    
    if  not modelchoice == None :
        choix = get_model4invertion()
        modelchoix = os.path.join(modeldir,"SavedModel_"+choix)
    else:
        modelchoix = modeldir
    # The following line creates an instance of the Superimpose application
    app5 = otbApplication.Registry.CreateApplication("InvertSARModel")
    app5.SetParameterInputImage("insarth", app2.GetParameterOutputImage("out") )
    app5.SetParameterInputImage("inndvi", app3.GetParameterOutputImage("out")) #ndvisup)
    app5.SetParameterInputImage("inlabels", app4.GetParameterOutputImage("out")) # labelssup)
    app5.SetParameterString("model.dir", modelchoix)
    
    if not slope == None and len(slope) > 2:
        app5.SetParameterInputImage("insarvv", appS.GetParameterOutputImage("out"))
    else:
        app5.SetParameterInputImage("insarvv", app1.GetParameterOutputImage("out"))
    
    if sarmode == "vh" :
        app5.SetParameterString("sarmode", sarmode)
        
    if outformat == "csv" :
        app5.SetParameterString("format", outformat)
        app5.SetParameterString("format.csv.out", output)
        print("Iverting model to csv with modeldir : "+ modelchoix)
        app5.ExecuteAndWriteOutput()
        print("Done \n")
    else:
        app5.SetParameterString("format", outformat)
        app5.SetParameterString("format.raster.out", "temp.tif")
        
        print("Iverting model to raster with modeldir : "+ modelchoix)
        # The following line execute the application
        app5.Execute()
        print("End of Ivertion \n")
        
        app6 = otbApplication.Registry.CreateApplication("BandMath")
        app6.AddImageToParameterInputImageList("il",app5.GetParameterOutputImage("format.raster.out"))
        app6.SetParameterString("out", "temp2.tif")
        app6.SetParameterString("exp", "im1b1*5")
        app6.SetParameterInt("ram",4000)
        
        print("Launching... BandMath(im1b1*5)")
        # The following line execute the application
        app6.Execute() #ExecuteAndWriteOutput() 
        print("End of BandMath \n")
    
    #    Ajouter ManageNoData    
        app7 = otbApplication.Registry.CreateApplication("ManageNoData")
        app7.SetParameterInputImage("in", app6.GetParameterOutputImage("out"))
        app7.SetParameterString("out", output+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")#
        app7.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
        app7.SetParameterString("mode", "changevalue")
        app7.SetParameterInt("ram",4000)
        
        
        print("Launching... ManageNoData")
        app7.ExecuteAndWriteOutput()
        print("Done \n")
    

    
def masked_pipeline():
    global maskedlabels
    #prepare temp files
    items = labels.split('.')
    items.insert(len(items)-1, "masked")
    maskedlabels = '.'.join(items)
    
    # The following line creates an instance of the Superimpose application
    app4 = otbApplication.Registry.CreateApplication("SoilMoistureSegmentationFiltering")
    
    # The following lines set all the application parameters:
#    app4.SetParameterString("labels", labels)
#    app4.SetParameterString("lulc", lulc)
    params = {"labels":labels, "lulc":lulc, "out":maskedlabels}
    if agrivalues != None:
        params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
#        app4.SetParameterListString("agrivalues", agrivalues)
#    app4.SetParameterString("out", maskedlabels)
    app4.SetParameters(params)
    
    print("Masking segmentation labels")
    # The following line execute the applicationw
    app4.ExecuteAndWriteOutput()
    print("End of Masking \n")
    
    short_pipeline(normalize_proj_and_extend())
    
def segmented_pipeline():
    global labels
    #prepare temp files
    items = ndvi.split('.')
    items.insert(len(items)-1, "labels")
    labels = '.'.join(items)
    
    # The following line creates an instance of the Superimpose application
    app4 = otbApplication.Registry.CreateApplication("SoilMoistureLULCMask")
    
    # The following lines set all the application parameters:
    app4.SetParameterString("inr", ndvi)
    app4.SetParameterString("inm", lulc)
    if agrivalues != None:
#        params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
        app4.SetParameterStringList("agrival", agrivalues.split(" "))
#    app4.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
#    app4.SetParameters(params)
    
    print("Masking NDVI stack")
    # The following line execute the applicationw
    app4.Execute()
    
    # The following line creates an instance of the Superimpose application
    app3 = otbApplication.Registry.CreateApplication("Segmentation")
    app3.SetParameterString("filter","meanshift")
    app3.SetParameterInt("filter.meanshift.spatialr", 5)
    app3.SetParameterFloat("filter.meanshift.ranger", 10)
#    app3.SetParameterInt("ram", 1024)
#    app3.SetParameterInt("tilesizex", 2000)
#    app3.SetParameterInt("tilesizey", 2000)
#    app3.SetParameterInt("spatialr", 5)
#    app3.SetParameterFloat("ranger", 10)
    app3.SetParameterString("mode","raster")
    app3.SetParameterInputImage("in", app4.GetParameterOutputImage("out"))
    app3.SetParameterString("mode.raster.out", labels)
    app3.SetParameterOutputImagePixelType("mode.raster.out", otbApplication.ImagePixelType_uint32)
    
    print("Segmentation of NDVI")
    # The following line execute the applicationw
    app3.ExecuteAndWriteOutput()
    print("End of Segmentation \n")
    
    masked_pipeline()

def labels_vector_pipeline():
    global labels
    #prepare temp files
    items = vectorlabels.split('.')
    items[len(items)-1]= "tif"
    labels = '.'.join(items)
    
    # The following line creates an instance of the Superimpose application
    app3 = otbApplication.Registry.CreateApplication("Rasterization")
    app3.SetParameterString("mode","attribute")
    app3.SetParameterString("mode.attribute.field", labelsfield)
    app3.SetParameterString("in", vectorlabels)
    app3.SetParameterString("im", ndvi)
    app3.SetParameterString("out", labels)
    
    print("Rasterizing segmentation vector")
    # The following line execute the applicationw
    app3.ExecuteAndWriteOutput()
    print("End of Rasterizing \n")
    
    masked_pipeline()
    
    

def ndvistackandmask_pipeline(ndvis,lulc,agrivalues, out):
    
    app0 = otbApplication.Registry.CreateApplication("ConcatenateImages")
        
    app0.SetParameterStringList("il",ndvis)    
    app0.SetParameterString("out", "temp0.tif")
#    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
#    app0.SetParameterInt("ram",4000)
    app0.Execute()
    
    
    # The following line creates an instance of the Superimpose application
    app4 = otbApplication.Registry.CreateApplication("SoilMoistureXBandLULCMask")
    
    # The following lines set all the application parameters:
    app4.SetParameterInputImage("inr", app0.GetParameterOutputImage("out"))
    app4.SetParameterString("inm", lulc)
    if agrivalues != None:
#        params = {"labels":labels, "lulc":lulc, "agrivalues":agrivalues.split(" "), "out":maskedlabels}
        app4.SetParameterStringList("agrival", agrivalues.split(" "))
    app4.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    app4.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")
#    app4.SetParameters(params)
    
    print("Masking NDVI stack")
    # The following line execute the applicationw
    app4.ExecuteAndWriteOutput()
    print("End of Masking \n")
    
    
def unzip_hgt_file(file, outdir):
    cmd='unzip -d '+outdir+' "'+file+'"'
    print("command : "+cmd)
    p = subprocess.Popen(cmd, shell=True)
    p.wait() 
    
def main():
    print( "hop")
    




if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Launch Soil Moisture pipeline with different level of processing.
        """)

    subparsers = parser.add_subparsers(help='Pipelines', dest="pipeline")

    # Short pipeline
    list_parser = subparsers.add_parser('short', help="Inversion pipeline from user's masked labels, NDVI, SarVV and SarIncidence")
    list_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find tensorflow model (dry or wet)')
    list_parser.add_argument('-sarvv', action='store', required=True, help='Sentinel-1 SAR VV image')
    list_parser.add_argument('-sarth', action='store', required=True, help='Sentinel-1 SAR incidence image')
    list_parser.add_argument('-ndvi', action='store', required=True, help='NDVI image computed from Sentinel-2')
    list_parser.add_argument('-maskedlabels', action='store', required=True, help='Masked segmented image computed from NDVI')
    list_parser.add_argument('-slope', action='store', required=False, help='[Optional] Slope image to filter SarVV')
    list_parser.add_argument('-slopevalue', action='store', required=False, help='[Optional] Max value of slope to use to filter SarVV, default 20')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    list_parser.add_argument('-output', action='store', required=True, help='Soil moisture map over agricultural areas')
    
    # Masked pipeline
    list_parser = subparsers.add_parser('mask', help="Mask labels image before inversion")
    list_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find tensorflow model (dry or wet)')
    list_parser.add_argument('-sarvv', action='store', required=True, help='Sentinel-1 SAR VV image')
    list_parser.add_argument('-sarth', action='store', required=True, help='Sentinel-1 SAR incidence image')
    list_parser.add_argument('-ndvi', action='store', required=True, help='NDVI image computed from Sentinel-2')
    list_parser.add_argument('-labels', action='store', required=True, help='Segmented image computed from NDVI')
    list_parser.add_argument('-lulc', action='store', required=True, help='LandUseLandCover raster to be used for labels masking (Default THEIA OSO)')
    list_parser.add_argument('-agrivalues', action='store', required=False, help='[Optional] List of agricultural areas values in LandUseLandCover raster, default "11 12"')
    list_parser.add_argument('-slope', action='store', required=False, help='[Optional] Slope image to filter SarVV')
    list_parser.add_argument('-slopevalue', action='store', required=False, help='[Optional] Max value of slope to use to filter SarVV, default 20')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    list_parser.add_argument('-output', action='store', required=True, help='Soil moisture map over agricultural areas')

    # Segmented pipeline
    list_parser = subparsers.add_parser('segment', help="Segment NDVI before masking and inversion")
    list_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find tensorflow model (dry or wet)')
    list_parser.add_argument('-sarvv', action='store', required=True, help='Sentinel-1 SAR VV image')
    list_parser.add_argument('-sarth', action='store', required=True, help='Sentinel-1 SAR incidence image')
    list_parser.add_argument('-ndvi', action='store', required=True, help='NDVI image computed from Sentinel-2')
    list_parser.add_argument('-lulc', action='store', required=True, help='LandUseLandCover raster to be used for labels masking (Default THEIA OSO)')
    list_parser.add_argument('-agrivalues', action='store', required=False, help='[Optional] List of agricultural areas values in LandUseLandCover raster, default "11 12"')
    list_parser.add_argument('-slope', action='store', required=False, help='[Optional] Slope image to filter SarVV')
    list_parser.add_argument('-slopevalue', action='store', required=False, help='[Optional] Max value of slope to use to filter SarVV, default 20')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    list_parser.add_argument('-output', action='store', required=True, help='Soil moisture map over agricultural areas')
    
    # Segmented vector pipeline
    list_parser = subparsers.add_parser('segmentvect', help="Rasterize segmented NDVI vector before masking and inversion")
    list_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find tensorflow model (dry or wet)')
    list_parser.add_argument('-sarvv', action='store', required=True, help='Sentinel-1 SAR VV image')
    list_parser.add_argument('-sarth', action='store', required=True, help='Sentinel-1 SAR incidence image')
    list_parser.add_argument('-ndvi', action='store', required=True, help='NDVI image computed from Sentinel-2')
    list_parser.add_argument('-vectorlabels', action='store', required=True, help='Segmention shapefile')
    list_parser.add_argument('-labelsfield', action='store', required=True, help='Field name from attribute table storing segmentation labels (Default "DN")')
    list_parser.add_argument('-lulc', action='store', required=True, help='LandUseLandCover raster to be used for labels masking (Default THEIA OSO)')
    list_parser.add_argument('-agrivalues', action='store', required=False, help='[Optional] List of agricultural areas values in LandUseLandCover raster, default "11 12"')
    list_parser.add_argument('-slope', action='store', required=False, help='[Optional] Slope image to filter SarVV')
    list_parser.add_argument('-slopevalue', action='store', required=False, help='[Optional] Max value of slope to use to filter SarVV, default 20')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    list_parser.add_argument('-output', action='store', required=True, help='Soil moisture map over agricultural areas')
    
    # SAR Series pipeline
    list_parser = subparsers.add_parser('serie', help="Batch inversion pipeline over a directory of Sentinel-1 images")
    list_parser.add_argument('-modeldir', action='store', required=True, help='Directory to find both wet and dry tensorflow model')
    list_parser.add_argument('-modelchoice', action='store', required=False, help='[Optional] Results text file from gpm pipeline')
    list_parser.add_argument('-sardir', action='store', required=True, help='Directory to find Sentinel-1 images ')
    list_parser.add_argument('-sardirtype', choices=['snap', 's2tile'],  default='s2tile', required=False, help='[Optional] Choose between Sentinel-1 images processed by ESA-SNAP software ("Sigma0_VV.img" and "incidenceAngleFromEllipsoid.img" in .data directory) \
                                                                                                                 AND Sentinel-1 images processed by SoilMoistureBatchExtractAndMosaic2S2Tile.py')
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Monthly NDVI image directory computed from Sentinel-2 (regex: "NDVI_XXXXXX_AAAAMM.TIF)')
    list_parser.add_argument('-maskedlabels', action='store', required=True, help='Masked segmented image computed from NDVI')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-slope', action='store', required=False, help='[Optional] Slope image to filter SarVV')
    list_parser.add_argument('-slopevalue', action='store', required=False, help='[Optional] Max value of slope to use to filter SarVV, default 20')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-outformat', choices=['raster', 'csv'],  default='raster', required=False, help='[Optional] Format of output when calling otbcli_InvertSARModel, cvs or raster')
    list_parser.add_argument('-outdir', action='store', required=True, help='Directory of Soil moisture map over agricultural areas results')
    
    # Stack and mask before segm pipeline
    list_parser = subparsers.add_parser('stack', help="Create stacked and masked image from NDVI directory to the input of a segmentation process")
    list_parser.add_argument('-ndvidir', action='store', required=True, help='Directory to find all the NDVI file to stack. Files naming must contain "NDVI" and ".tif" extension')
    list_parser.add_argument('-lulc', action='store', required=True, help='LandUseLandCover raster to be used for labels masking (Default THEIA OSO)')
    list_parser.add_argument('-agrivalues', action='store', required=False, help='[Optional] List of agricultural areas values in LandUseLandCover raster, default "11 12"')
    list_parser.add_argument('-output', action='store', required=True, help='Stacked and masked NDVIs output file name')
    
    # Extract SRTM1SecHGT from Snap download and compute slope
    list_parser = subparsers.add_parser('slope', help="Extract SRTM1SecHGT from Snap download and compute slope")
    list_parser.add_argument('-srtmdir', action='store', required=True, help='Snap DEM SRTM 1Sec HGT directory containing zip file')
    list_parser.add_argument('-inref', action='store', required=True, help='Image reference for DEM extraction in projected reference system')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic Sentinel2 zone reference for output files naming')
    list_parser.add_argument('-demtype', choices=['srtmhgt1sec', 'srtm3sec'],  default='srtmhgt1sec', required=False, help='[Optional] Choose between SRTM HGT 1sec or SRMT 3sec, default srtmhgt1sec')
    list_parser.add_argument('--no-deletedem', dest='deletedem', action='store_false', help='Avoid deleting DEM file used to produce slope (default True)')
    list_parser.set_defaults(deletedem=True)
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory for slope image')
    
    # Select wet or dry model for inversion from GPM data
    list_parser = subparsers.add_parser('gpm', help="Downloads, extracts and computes synthesis of NASA Global Precipitation Model raster for TF model selection (wet or dry)")
    list_parser.add_argument('-sardir', action='store', required=True, help='Directory to find Sentinel-1 images')
    list_parser.add_argument('-sardirtype', choices=['snap', 's2tile'],  default='s2tile', required=False, help='[Optional] Choose between Sentinel-1 images processed by ESA-SNAP software ("Sigma0_VV.img" and "incidenceAngleFromEllipsoid.img" in .data directory) \
                                                                                                                 AND Sentinel-1 images processed by SoilMoistureBatchExtractAndMosaic2S2Tile.py')
    list_parser.add_argument('-sarmode', choices=['vv', 'vh'],  default='vv', required=False, help='[Optional] Choose between VV or VH mode for input SAR image, default vv')
    list_parser.add_argument('-gpmdir', action='store', required=True, help='Directory where GPM data will be downloaded, autodetect previous downloads')
    list_parser.add_argument('-zone', action='store', required=True, help='Geographic zone reference for output files naming')
    list_parser.add_argument('-outtxt', action='store', required=True, help='Output text file where each Sentinel-1 dates correspond their model dry or wet to use')
    list_parser.add_argument('--no-resampling', dest='resampling', action='store_false', help='[Optional] Resample GPM 100km to 10m, default false')
    list_parser.set_defaults(resampling=False)

    args=parser.parse_args()

    # Apply CSV file on Labels file
    list_parser = subparsers.add_parser('csvtomoist', help="Apply CSV results on labels raster to create moisture map")
    list_parser.add_argument('-csvdir', action='store', required=True, help='Directory to find all the NDVI file to stack. Files naming must contain "NDVI" and ".tif" extension')
    list_parser.add_argument('-maskedlabels', action='store', required=True, help='Masked segmented image computed from NDVI')
    list_parser.add_argument('-outdir', action='store', required=True, help='Output directory')
    
    args=parser.parse_args()
    
    if args.pipeline == 'short' :
        modeldir =  args.modeldir
        sarvv = args.sarvv
        sarth = args.sarth
        ndvi = args.ndvi
        maskedlabels = args.maskedlabels
        output = args.output
        slope = args.slope
        slopevalue = args.slopevalue
        sarmode = args.sarmode
        outformat = args.outformat
        print("Starting Short pipeline")
        
        short_pipeline(normalize_proj_and_extend())
         
        
    elif args.pipeline == 'mask' :
        modeldir =  args.modeldir
        sarvv = args.sarvv
        sarth = args.sarth
        ndvi = args.ndvi
        labels = args.labels
        lulc = args.lulc
        agrivalues = args.agrivalues
        output = args.output
        slope = args.slope
        slopevalue = args.slopevalue
        sarmode = args.sarmode
        outformat = args.outformat
        print("Starting Masked pipeline")
        masked_pipeline()
        
    elif args.pipeline == 'segment' :
        modeldir =  args.modeldir
        sarvv = args.sarvv
        sarth = args.sarth
        ndvi = args.ndvi
        lulc = args.lulc
        agrivalues = args.agrivalues
        output = args.output
        slope = args.slope
        slopevalue = args.slopevalue
        sarmode = args.sarmode
        outformat = args.outformat
        print("Starting Segmented pipeline")
        segmented_pipeline()
    
    elif args.pipeline == 'segmentvect' :
        modeldir =  args.modeldir
        sarvv = args.sarvv
        sarth = args.sarth
        ndvi = args.ndvi
        vectorlabels = args.vectorlabels
        labelsfield = args.labelsfield
        lulc = args.lulc
        agrivalues = args.agrivalues
        output = args.output
        slope = args.slope
        slopevalue = args.slopevalue
        sarmode = args.sarmode
        outformat = args.outformat
        print("Starting Segment vector pipeline")
        labels_vector_pipeline()
        
    elif args.pipeline == 'csvtomoist' :
        maskedlabels = args.maskedlabels
        if not os.path.isdir(args.csvdir):
            print("erreur "+args.csvdir+" n'est pas un dossier")
            exit()
        if not os.path.exists(args.outdir):
            os.mkdir(args.outdir)
        
        csvs=search_files(args.csvdir, 'MV', 'csv', 'f')
        if len(csvs)==0 :
            print("No CSV file found.")
            exit()
            
        for csv in csvs:
            outmoist = os.path.join(args.outdir,os.path.basename(csv)[:-3]+"TIF")
            print("\nProcessing file: "+csv)
            csvtoraster_pipeline(csv,outmoist)
    
    elif args.pipeline == 'serie' :
        
        if not os.path.isdir(args.sardir):
            print("erreur "+args.sardir+" n'est pas un dossier")
            exit()
        if not os.path.isdir(args.ndvidir):
            print("erreur "+args.ndvidir+" n'est pas un dossier")
            exit()
        if not os.path.isdir(args.outdir):
            print("erreur "+args.outdir+" n'est pas un dossier")
            exit()

        sars=[]
        if args.sardirtype == "snap":
            sars=search_files(args.sardir, 'S1', 'data', 'd')
        elif args.sardirtype == "s2tile":
            if args.sarmode == "vv":
                sars=search_files(args.sardir, 'S1', 'VV.TIF', 'f')
            elif args.sarmode == "vh":
                sars=search_files(args.sardir, 'S1', 'VH.TIF', 'f')
            else:
                print("Error: Exception on sarmode")
                exit()
        else:
            print("ERROR: wrong sardirtype argument")
            exit()
            
        ndvis=[]
        ndvis=search_files(args.ndvidir)
        
        modeldir =  args.modeldir
        modelchoice =  args.modelchoice
        maskedlabels = args.maskedlabels
        slope = args.slope
        slopevalue = args.slopevalue
        sarmode = args.sarmode
        outformat = args.outformat
        print("Starting SAR serie pipeline")
        
        for sard in sars:
            print("#################################################################################### ")
            print("Resolving filenames for " + sard)
            
            #TODO find S1A or S1B
            
            sardata = os.path.basename(sard)
            fdate = re.findall("20\d{6}T\d{6}",sardata)
            if not(fdate):
                print("Impossible de trouver la date dans le nom de fichier : "+n)
                exit
            sarsplit = sardata.split('_')
            sardate = sarsplit[4].split('T')
            sardate = fdate[0].split('T')
            annee =sardate[0][:4]
            mois =sardate[0][4:6]
            print(annee+" / "+mois)
            
            ndvi="tjs rien"
            
            l = [ndvis.index(i) for i in ndvis if annee+mois in i]
            print("result search =")
            print(l)
            if len(l) > 0 :
                ndvi = ndvis[l[0]]
                    
            if ndvi == "tjs rien":
                print("Search for closest NDVI")
                for m in range(6):
                    m+=1
                    moisprecn = (int(mois) - m) % 12
                    moissuivn = (int(mois) + m) % 12
                    anneeprec = annee
                    anneesuiv = annee
                    
                    if moisprecn == 0 :
                        moisprecn = 12
                        anneeprec = str(int(annee)-1)
                    if moissuivn == 1 :
                        anneesuiv = str(int(annee)+1)
                    
                    moisprec = str(moisprecn)
                    moissuiv = str(moissuivn)
                        
                    if moisprecn < 10 :
                        moisprec ="0"+str(moisprecn)
                    if moissuivn < 10 :
                        moissuiv ="0"+str(moissuivn)
                        
                    
                    l = [ndvis.index(i) for i in ndvis if anneeprec+moisprec in i]
                    
                    print("result search =")
                    print(l)
                    if len(l) > 0 :
                        ndvi = ndvis[l[0]]
                        break
                    
                    l = [ndvis.index(i) for i in ndvis if anneesuiv+moissuiv in i]
                    
                    print("result search =")
                    print(l)
                    if len(l) > 0 :
                        ndvi = ndvis[l[0]]
                        break
            
            if args.sardirtype == "snap":
                if args.sarmode == "vv":
                    sarvv = os.path.join(sard , "Sigma0_VV.img")
                elif args.sarmode == "vh":
                    sarvv = os.path.join(sard , "Sigma0_VH.img")
                else:
                    print("Error: Exception on sarmode")
                    exit()
                sarth = os.path.join(sard , "incidenceAngleFromEllipsoid.img")
            else:
                sarvv = sard
                sarbase = os.path.dirname(sarvv)
                if os.path.exists(os.path.join(sarbase,"auxfiles")) :
                    auxdir = os.path.join(sarbase,"auxfiles")
                    thfname = os.path.basename(sard).replace("_VV.TIF","_THETALOC.TIF")
                    sarth = os.path.join(auxdir,thfname)
                else:
                    sarth = sard[:-6]+"THETA.TIF"
            
            if args.outformat == "raster":
                output = os.path.join(args.outdir, "MV_"+sarsplit[0]+"_"+args.zone+"_"+sarsplit[4]+".TIF") 
            elif args.outformat == "csv":
                output = os.path.join(args.outdir, "MV_"+sarsplit[0]+"_"+args.zone+"_"+sarsplit[4]+".csv") 
            else:
                print("Error: Exception on sarmode")
                exit()
            
            print("using: \n"+sarvv+" \n"+sarth+" \n"+ndvi+" \n"+output)
            
            short_pipeline(normalize_proj_and_extend())
        
    elif args.pipeline == 'stack' :
        
        if not os.path.isdir(args.ndvidir):
             print("erreur "+args.ndvidir+" n'est pas un dossier")
             exit()
        
        ndvis=[]
        ndvis=search_files(args.ndvidir)
       
        lulc = args.lulc
        agrivalues = args.agrivalues
        output = args.output
        print("Starting stack and mask pipeline")
        ndvistackandmask_pipeline(ndvis,lulc,agrivalues, output)
        
    elif args.pipeline == 'slope':
        
        #get tiles
        raster = gdal.Open(args.inref)
        proj = osr.SpatialReference(wkt=raster.GetProjection())
        
        if proj.IsGeographic() :
            
            print("Reference image is in Geographic coordinate system. Reprojection is needed.")
            exit()

        upx, xres, xskew, upy, yskew, yres = raster.GetGeoTransform()
        cols = raster.RasterXSize
        rows = raster.RasterYSize
         
        ulx = upx + 0*xres + 0*xskew
        uly = upy + 0*yskew + 0*yres
         
        llx = upx + 0*xres + rows*xskew
        lly = upy + 0*yskew + rows*yres
         
        lrx = upx + cols*xres + rows*xskew
        lry = upy + cols*yskew + rows*yres
         
        urx = upx + cols*xres + 0*xskew
        ury = upy + cols*yskew + 0*yres
        
        outSpatialRef = osr.SpatialReference()
        outSpatialRef.ImportFromEPSG(4326)
        coordTransform = osr.CoordinateTransformation(proj, outSpatialRef)
        
        pointLL, pointUR = ogr.Geometry(ogr.wkbPoint), ogr.Geometry(ogr.wkbPoint)
        pointLL.AddPoint(llx, lly)
        pointUR.AddPoint(urx, ury)
        pointLL.Transform(coordTransform)
        pointUR.Transform(coordTransform)
        
        tilesname=[]
        print("LowerLeft="+pointLL.ExportToWkt()+";UpperRight="+pointUR.ExportToWkt())
        
        for o in range(math.ceil(pointUR.GetX()) - math.floor(pointLL.GetX())):
            for a in range(math.ceil(pointUR.GetY()) - math.floor(pointLL.GetY())):
                lat =  math.floor(pointLL.GetY()) + a
                lon =  math.floor(pointLL.GetX()) + o
                if lat >= 0 :
                    hem = 'N'
                else : 
                    hem = 'S'
                    lat = abs(lat)
                if lon >= 0 :
                    grw = 'E'
                else : 
                    grw = 'W'
                    lon = abs(lon)
                tilesname.append(hem+f'{lat:02}'+grw+f'{lon:003}')
        
        print("Unziping tiles: "+str(tilesname))
        tilefiles = []
        if len(tilesname) == 0:
            print("bug")
            exit()
            
        if args.demtype == "srtmhgt1sec":
            srtmsuf = ".SRTMGL1.hgt.zip"
        elif args.demtype == "srtm3sec":
            srtmsuf = ".SRTM.zip"
        else:
            print("wrong argument demtype!")
            exit()
            
        for tile in tilesname :
            tilef = os.path.join(args.srtmdir,tile + srtmsuf)
            if not os.path.exists(tilef):
                print("Missing SRTM tile : "+tilef)
                continue
            unzip_hgt_file(tilef,args.outdir)
            tilefiles.append(os.path.join(args.outdir,tile+".hgt"))
            
        print("Using tile files: ")
        [print(tilefile) for tilefile in tilefiles]
        outdem = os.path.join(args.outdir,"SRTM_"+args.zone+".TIF")
        mosapp = otbApplication.Registry.CreateApplication("Mosaic")
        mosparams = {"il":tilefiles, "out":"mosatemp.tif"}
        mosapp.SetParameters(mosparams)
        mosapp.Execute()
        
        supapp = otbApplication.Registry.CreateApplication("Superimpose")
        supapp.SetParameterString("inr", args.inref)
        supapp.SetParameterInputImage("inm", mosapp.GetParameterOutputImage("out"))
        supapp.SetParameterString("out", outdem)
        supapp.ExecuteAndWriteOutput()
                
        #Compute slope
        print("Generating slope file...")
        outslope = os.path.join(args.outdir,"SLOPE_"+args.zone+".TIF")
        cmd='gdaldem slope -p '+outdem+' '+outslope
        print("command : "+cmd)
        p = subprocess.Popen(cmd, shell=True)
        p.wait() 
        
        #Cleaning files
        print("Cleaning files")
        for tile in tilefiles :
            os.remove(tile)
            os.remove(tile[:-3]+"omd")
        if args.deletedem:
            os.remove(outdem)
        
        print("Done.")
    
    elif args.pipeline == 'gpm':
        wet_or_dry_pipeline(args)
           
        
    else :
        parser.exit()