#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 11:47:04 2019

@author: -
"""


import matplotlib.pyplot as plt
import datetime, argparse, os


#def create_graph(ndvidates,ndvis,outgraph):
#    x = ndvidates
#    y = ndvis
#    
#    fig = go.Figure(data=[go.Scatter(x=x, y=y)])
#    # Use datetime objects to set xaxis range
#    fig.update_layout(xaxis_range=[ndvidates[0],
#                                   ndvidates[-1]])
##    fig.show()
#    fig.write_image(outgraph)


if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Processing cvs files from InvertSarModel for graphic creation on NDVI
        """)
    
    parser.add_argument('-csvfiles',type=argparse.FileType('r'), nargs='+', help='List of processed csv files')
    parser.add_argument('-outdir', action='store', required=True, help='Output graphic images')
#    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
#    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    
    
    args=parser.parse_args()
#    filesparams=[]
    parcelidsmap={}
    for f in args.csvfiles:
#        filesparams.append([f,f.name.split("_")[0],f.name.split("_")[1]])
        i=0
        for line in f:
            i+=1
            if i==1:
                continue
            linesp = line.split(";")
            ndvidatesp = linesp[8].split("-")
            parcelidsmap.setdefault(linesp[0],{}).setdefault(
                f.name.split("_")[2][:-4],[]).append([datetime.date(int(ndvidatesp[0]),int(ndvidatesp[1]),int(ndvidatesp[2])),float(linesp[3])])
            
            parcelidsmap[linesp[0]]["CODE_GROUP"] = linesp[6]
            parcelidsmap[linesp[0]]["CODE_CULTU"] = linesp[7]
            parcelidsmap[linesp[0]]["NbPixels"] = linesp[1]
    
    styles = {
            "raw":"b.-",
            "linear":"r.-",
            "spline":"g.-"
            }       
    for parcelid in parcelidsmap:
        plt.figure(figsize=[15.,8.])
        gapfils = parcelidsmap[parcelid]
        for gapfil in gapfils:
            if gapfil == "CODE_GROUP" or gapfil == "CODE_CULTU" or gapfil == "NbPixels" :
                continue
            gapfils[gapfil].sort(key=lambda tup: tup[0])
            
            x = [row[0] for row in gapfils[gapfil]]
            y = [row[1] for row in gapfils[gapfil]]
            
            if parcelid == '1918334':
                print(x)
                print(y)
            # plot
            plt.plot(x,y,styles[gapfil],label=gapfil)
            # beautify the x-labels
            
            
        
        # Edit the layout
        
        plt.gcf().autofmt_xdate()
        plt.title('Parcel='+parcelid+', Group='+gapfils["CODE_GROUP"]+
                          ', Culture='+gapfils["CODE_CULTU"]+', NbPixels='+gapfils["NbPixels"])
        plt.ylabel('NDVI')
        plt.xlabel('Dates')
        plt.legend()
        outgraph = os.path.join(args.outdir, "GROUP_"+gapfils["CODE_GROUP"]+"_"+gapfils["CODE_CULTU"]+"_"+parcelid+".png")
        plt.savefig(outgraph)  
#        plt.show()  
        plt.close()
        
    