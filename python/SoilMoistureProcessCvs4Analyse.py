#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 25 09:50:31 2019

@author: -
"""
from osgeo import ogr
import os,datetime
import argparse, random


def search_files(directory='.', extension='csv', resolution='MV'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 :
                
                #print(os.path.join(dirpath, name))
                abspath = os.path.abspath(os.path.join(dirpath, name))
                images.append(abspath)

    print(str(len(images)) + " csv files found")
    return images


if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Processing cvs files from InvertSarModel for graphic creation on NDVI
        """)
    
    parser.add_argument('-csvdir', action='store', required=True, help='Directory containing time serie csv files')
    parser.add_argument('-ndvidate', action='store', required=True, help='NDVI date file from SoilMoistureGapfilling')
    parser.add_argument('-rpgshp', action='store', required=False, help='[Optional] "Reseau Parcellaire Graphique" Shapefile cropped on S2 tile')
    parser.add_argument('-codevalues', action='store', default='1 2 3 4', required=False, help='[Optional] CODE_GROUP field values in "Reseau Parcellaire Graphique" Shapefile to sample on, default "1 2 3 4"')
    parser.add_argument('-ref', action='store', required=False, help='[Optional] Previous csv file from ProcessCsv4Analyse to get sampled parcel ids')
    parser.add_argument('-outcsv', action='store', required=True, help='Output csv file')
#    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
#    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    
    
    args=parser.parse_args()
    
    rpgshpvalues = args.codevalues.split(" ")
    if len(rpgshpvalues) == 0 : 
        print("ERROR: wrong argument on rpgshpvalues")
        exit
    
    #Find csv by ndvidate
    #TODO le faire réelement et filtrer les parcelids avec
    print("Finding cvs files corresponding to NDVI dates")
    csvfiles=search_files(args.csvdir)
    csvdates=[]
    for csv in csvfiles:
        csvsplit = os.path.basename(csv).split('_')
        csvdate = datetime.date(int(csvsplit[3][:4]),int(csvsplit[3][4:6]),int(csvsplit[3][6:8]))
        csvdates.append([csvdate,csv])
    
    csvatndvi=[]
    with open(args.ndvidate, 'r') as ndvidates:
        for dline in ndvidates:
            ndvidate = datetime.date(int(dline[:4]),int(dline[4:6]),int(dline[6:8]))
            deltas = []
            for csvdate in csvdates:
                deltas.append( [abs(csvdate[0] - ndvidate), csvdate])
                
            deltas.sort(key=lambda tup: tup[0])
#                [print(deltas[t]) for t in range(10)]
            #dline comprend un retour de chariot
            csvatndvi.append([deltas[0][1][1],ndvidate])
    
    samples=[]
    
    
    #Find labelids to use in random way
    if (args.rpgshp == None and args.ref == None ) or (not args.rpgshp == None and not args.ref == None):
        print("ERROR: Only one arguments betwenn -rpgshp and -ref must be filled!" )
        exit
    elif not args.rpgshp == None:
        print("Filtering and sampling shapefile")
        inShapefile = args.rpgshp
        inDriver = ogr.GetDriverByName("ESRI Shapefile")
        inDataSource = inDriver.Open(inShapefile, 0)
        inLayer = inDataSource.GetLayer()
        
        parcelidsok = {}
        for csv in csvatndvi :
            with open(csv[0], 'r') as test:
                for tline in test:
                    parcelidsok.setdefault(ndvidate, []).append(tline.split(";")[0])
        
        for code in rpgshpvalues:
            
            inLayer.SetAttributeFilter("CODE_GROUP = '"+code+"'")
            
            k=0
            while k < 10 :
                rdfeat = random.choice(inLayer)
                ok4alldate=0
                for datekey in parcelidsok:
                    if rdfeat.GetField("ID_PARCEL") in parcelidsok[datekey] :
                        ok4alldate+=1
                if len(parcelidsok) == ok4alldate :
                    samples.append([code,rdfeat.GetField("ID_PARCEL"),rdfeat.GetField("CODE_CULTU")])
                    k+=1
    
    elif not args.ref == None :
        print("Get Parcels ids from last csv processed")
        with open(args.ref, 'r') as csvref:
            i=0
            for rline in csvref:
                i+=1
                if i == 1:
                    continue
                csvrefsp = rline.split(";")
                parcelids = [row[1] for row in samples]
                if not csvrefsp[0] in parcelids:
                    samples.append([csvrefsp[6],csvrefsp[0],csvrefsp[7]])
            
    
    
    print("Writing output")
    parcelids = [row[1] for row in samples]
    with open(args.outcsv,'w') as out:
        out.write("Labels;NbPixels;Incidence;NDVI;SAR;MV;CODE_GROUP;CODE_CULTU;DATE\n")
        for csv in csvatndvi :
            with open(csv[0], 'r') as cc:
                i,k=0,0
                for linevalues in cc:
                    i+=1
                    if i == 1 :
                        continue
                    if k == len(parcelids) :
                        break
                    linevaluessp = linevalues.split(';')
                    for ind in range(len(parcelids)):
                        if int(linevaluessp[0]) == int(parcelids[ind]):
                            k+=1
                            res = ';'.join(linevaluessp[:-1])+";"+samples[ind][0]+";"+samples[ind][2]+";"+csv[1].strftime("%Y-%m-%d")
#                                if k == 3 :
#                                    print("??????????????????????????????????????????????????????????????????????????")
#                                    print(res)
#                                    print("##########################################################################")
                                
                            out.write(res+"\n")
                        
        
    
        
            
        
        
        
        
    