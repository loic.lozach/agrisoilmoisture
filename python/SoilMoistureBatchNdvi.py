#!/usr/bin/python
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


import os
import argparse
import otbApplication

def search_files(directory='.', extension='jp2', resolution='10m', band='B04'):
    images=[]
    extension = extension.lower()
    resolution = resolution.lower()
    band = band.upper()
    
    for dirpath, dirnames, files in os.walk(directory):
        for name in files:
            if extension and name.lower().endswith(extension) and name.lower().find(resolution) >= 0 and name.upper().find(band) > 0:
                
                #print(os.path.join(dirpath, name))
                abspath = os.path.abspath(os.path.join(dirpath, name))
                images.append(abspath)

    print(str(len(images)) + " image(s) found")
    return images

def ndvi_pipeline(argsformat, nir, red, mask, out):
    
    app0 = otbApplication.Registry.CreateApplication("Superimpose")
    app0.SetParameterString("inm",mask)
    app0.SetParameterString("inr",nir)
    app0.SetParameterString("out", "temp0.tif")
    app0.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    app0.SetParameterString("interpolator", "nn")
#    app0.SetParameterInt("ram",4000)
    app0.Execute()
    
    app1 = otbApplication.Registry.CreateApplication("BandMath")
    # Define Input im2: Band Red (B4)
#    app1.AddParameterStringList("il",[nir,red])
    app1.AddParameterStringList("il",nir)
    app1.AddParameterStringList("il",red)
    app1.SetParameterString("out", "temp1.tif")
    app1.SetParameterString("exp", "(im1b1-im2b1)/(im1b1+im2b1)>0?(im1b1-im2b1)/(im1b1+im2b1)*100:0")
#    app1.SetParameterInt("ram",4000)
    app1.Execute()
    
    valuemask = "0"
    if argsformat == 'S2-L3A':
        valuemask = "4"
    elif argsformat == 'L8-OLI/TIRS':
        valuemask = "322 or im2b1==386 or im2b1==834 or im2b1==898 or im2b1==1346"
        
    
    app2 = otbApplication.Registry.CreateApplication("BandMath")
    app2.AddImageToParameterInputImageList("il",app1.GetParameterOutputImage("out"))
    # Define Input im2: Band Red (B4)
    app2.AddImageToParameterInputImageList("il", app0.GetParameterOutputImage("out"))
    app2.SetParameterString("out", "temp2.tif")
    app2.SetParameterString("exp", "(im2b1=="+valuemask+")?im1b1:0")
#    app2.SetParameterInt("ram",4000)
    app2.Execute()
    
    app3 = otbApplication.Registry.CreateApplication("ManageNoData")
    app3.SetParameterInputImage("in", app2.GetParameterOutputImage("out"))
    app3.SetParameterString("out", out+"?gdal:co:COMPRESS=DEFLATE&gdal:co:BIGTIFF=YES")#
    app3.SetParameterOutputImagePixelType("out", otbApplication.ImagePixelType_uint8)
    app3.SetParameterString("mode", "changevalue")
#    app3.SetParameterInt("ram",4000)
    
    app3.ExecuteAndWriteOutput()

if __name__ == "__main__":
    # Make parser object
    parser = argparse.ArgumentParser(description=
        """
        Compute cloud-masked NDVI from Sentinel2 or Landsat8.
        """)
    
    parser.add_argument('-s2l2dir', action='store', required=True, help='Directory containing Sentinel2 L2A dimap directories')
    parser.add_argument('-format', choices=['S2-MUSCATE','S2-SEN2COR','S2-L3A','L8-OLI/TIRS'], required=True)
    parser.add_argument('-ndvidir', action='store', required=True, help='Output directory for computed NDVIs')
#    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
#    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    
    
    args=parser.parse_args()
    
    if args.format == "S2-SEN2COR" :
        
        bands4=[]
        bands4=search_files(args.s2l2dir)
    
        masks=[]
        masks=search_files(args.s2l2dir, 'jp2', resolution='20m', band='CLD')
        
    elif args.format == "S2-MUSCATE" :
        
        bands4=[]
        bands4=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_FRE_B4')
    
        masks=[]
        masks=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_CLM_R2')
        
    elif args.format == "S2-L3A" :
        
        bands4=[]
        bands4=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_FRC_B4')
    
        masks=[]
        masks=search_files(args.s2l2dir, 'tif', resolution='SENTINEL2', band='_FLG_R2')
        
    elif args.format == "L8-OLI/TIRS" :
        
        bands4=[]
        bands4=search_files(args.s2l2dir, 'tif', resolution='LC08_L1TP', band='_sr_band3')
    
        masks=[]
        masks=search_files(args.s2l2dir, 'tif', resolution='LC08_L1TP', band='_pixel_qa')
        
    else:
        print("S2 format not recognized!")
        exit
    
    if not os.path.isdir(args.ndvidir):
        os.mkdir(args.ndvidir)
    
    abspathout = os.path.abspath(args.ndvidir)
    
    for file in bands4:
#        splitfile = file.split("/")
#        splitfile.pop(0)
#        bandred = "/".join(splitfile)
        bandred = file
        splitfile = bandred.upper().split("/")
        
        if args.format == "S2-SEN2COR" :
            bandnir = bandred.replace('B04', 'B08')
            ind = splitfile.index("GRANULE")
            l = [masks.index(i) for i in masks if splitfile[ind+1] in i]
            splitname = os.path.basename(bandred).split('_')
            indband = splitname.index("B04")            
            if indband == 2:
                outfile = "NDVI_"+splitname[0]+"_"+splitname[1]+".TIF"
            elif indband == 3:
                outfile = "NDVI_"+splitname[1]+"_"+splitname[2]+".TIF"
            else:
                print("??????????????????????????????????????????????????????")
                print("ERROR: "+bandred)
                print("??????????????????????????????????????????????????????")
                continue
            
        elif args.format == "S2-MUSCATE" or args.format == "S2-L3A" :
            bandnir = bandred.replace('B4', 'B8')
#            ind = splitfile.index("MASKS")
            l = [masks.index(i) for i in masks if splitfile[len(splitfile)-2] in i]
            splitname = os.path.basename(bandred[:-4]).split('_')
            indband = splitname.index("B4")            
            if indband == 7:
                imgdate = splitname[1].split("-")
                outfile = "NDVI_"+splitname[3]+"_"+"T".join(imgdate[:2])+".TIF"
            else:
                print("??????????????????????????????????????????????????????")
                print("ERROR: "+bandred)
                print("??????????????????????????????????????????????????????")
                continue
        
        if args.format == "L8-OLI/TIRS" :
            bandnir = bandred.replace('band3', 'band5')
#            ind = splitfile.index("GRANULE")
            l = [masks.index(i) for i in masks if splitfile[-2] in i]
            splitname = os.path.basename(bandred).split('_')
#            indband = splitname.index("band3")            
            outfile = "NDVI_"+splitname[0]+"_"+splitname[2]+"_"+splitname[3]+".TIF"
            
       
#        outfile = os.path.basename(bandred.replace('B04_10m.jp2', 'NDVI_10m.tif'))
        absoutfile = os.path.join(abspathout, outfile)
        
        print("###########################################################")
        print("using red = " + bandred)
        print("using nir = " + bandnir)
        print("using mask = " + masks[l[0]])
        print("using out = " + absoutfile)
        print("###########################################################")
        
        
        
        ndvi_pipeline(args.format, bandnir, bandred, masks[l[0]], absoutfile)
        
    
    
    # Make parser object
#    parser = argparse.ArgumentParser(description=
#        """
#        Prepare Sentinel from Copernicus cart file in current directory.
        #""")
#
#    parser.add_argument('-cartfile', action='store', required=True, help='SciHub Copernicus Cart File (products.meta4)')
#    parser.add_argument('-user', action='store', required=True, help='SciHub Copernicus user name')
#    parser.add_argument('-psswd', action='store', required=True, help='SciHub Copernicus user password')
    
    
#    args=parser.parse_args()
#    
#    print "reading file : "+args.cartfile
#    
#    file = open(args.cartfile, 'r')
#    xmlstring = file.read()
#    file.close()
#
#    xml_string = re.sub(' xmlns="[^"]+"', '', xmlstring, count=1)
#    
#    tree = ET.fromstring(xml_string)
#    root = tree.getroot()
    
#    ns = {'b':"urn:ietf:params:xml:ns:metalink"}
    
#    for file in tree.findall('file'): #b:,ns
#        
#        name = file.get('name')
#        dataset = name.replace('.zip', '.SAFE')
#        if not ( os.path.isdir(dataset) and dataset.startswith('S2') and dataset.find('L1C') ):
#            continue
#        
#        my_env = os.environ.copy()
#        my_env["PATH"] = "/work/esa-snap/Sen2Cor-02.05.05-Linux64/bin:" + my_env["PATH"]    
#        cmd='source /work/esa-snap/Sen2Cor-02.05.05-Linux64/L2A_Bashrc && L2A_Process '+dataset
#        print "command : "+cmd
#        p = subprocess.Popen(cmd, shell=True, env=my_env)
#        p.wait() 
#        
#        print "Sen2Cor using file : "+name
#        
#    print "Done!"
        
