#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbWrapperCompositeApplication.h"
#include "itkMaskImageFilter.h"
#include "itkCastImageFilter.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
#include "otbHumidityFunctors.h"
#include "otbVectorDataIntoImageProjectionFilter.h"
#include "otbVectorDataToLabelImageFilter.h"
#include "otbConnectedLabelsImageFilter.h"
// Resample filter
#include "otbGenericRSResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"

namespace otb
{
namespace Wrapper
{
class SoilMoistureSegmentationFiltering : public CompositeApplication
{
public:
  typedef SoilMoistureSegmentationFiltering             Self;
  typedef CompositeApplication                Superclass;
  typedef itk::SmartPointer<Self>    Pointer;

  itkNewMacro(Self);
  itkTypeMacro(SoilMoistureSegmentationFiltering, otb::Wrapper::CompositeApplication);

  /** Pipeline */
  typedef UInt8ImageType MaskImageType;
  /** Typedefs for image reprojection */
 
  
  typedef otb::Functor::ThresholdFunctor<FloatImageType::PixelType,FloatImageType::PixelType> ThresholdFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatImageType,FloatImageType,ThresholdFunctorType> ThresholdFunctorFilterType; 
  

  
private:
  void DoInit()
  {
    SetName("SoilMoistureSegmentationFiltering");
    SetDescription("Filter NDVI segmented image for agricultural areas");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Loïc Lozac'h");
    SetDocSeeAlso(" ");
    
    //Application
    ClearApplications();
    
    AddApplication("SoilMoistureLULCMask", "etape1", "Create mask from landuse (default Theia OSO)");
//    AddApplication("SoilMoistureEdgeAndThreshold", "etape2", "Apply edge extraction and threshold on mask");
    AddApplication("EdgeExtraction", "edge", "Apply edge extraction and threshold on mask");
    AddApplication("BinaryMorphologicalOperation", "erode", "Erosion");
    AddApplication("SoilMoistureFilters4Mask", "etape3", "Finalize Mask");
    
    // Input images
    ShareParameter("labels","etape1.inr");
    ShareParameter("lulc","etape1.inm");
    ShareParameter("agrivalues","etape1.agrival");
    ShareParameter("out","etape3.out");
    ShareParameter("ram","etape1.ram");
    
    Connect("edge.ram","etape1.ram");
    Connect("erode.ram","edge.ram");
    Connect("etape3.ram","erode.ram");
    
    
    SetDefaultOutputPixelType("out", ImagePixelType_uint32);
    //Applications parameters

    
    GetInternalApplication("erode")->SetParameterString("structype","ball", false);
    GetInternalApplication("erode")->SetParameterInt("xradius",1, false);
    GetInternalApplication("erode")->SetParameterInt("yradius",1, false);
    GetInternalApplication("erode")->SetParameterString("filter","erode", false);
    
    

  }

  void DoUpdateParameters()
  {
  }

 

  /*
   * Do the work
   */
  void DoExecute()
  {
      
    ExecuteInternal("etape1");
    
    
    GetInternalApplication("edge")->SetParameterInputImage("in",
                            GetInternalApplication("etape1")->GetParameterOutputImage("out"));
//    
    ExecuteInternal("edge");
   

    /* Threshold edge */
    otbAppLogINFO("Threshold edge");
    m_ThresholdFunctorFilter = ThresholdFunctorFilterType::New();
    m_ThresholdFunctorFilter->SetInput(
                static_cast<FloatImageType*>(GetInternalApplication("edge")->GetParameterOutputImage("out")));
    m_ThresholdFunctorFilter->UpdateOutputInformation();

    /* Erode thresholded edge */
    otbAppLogINFO("Erode thresholded edge");
    GetInternalApplication("erode")->SetParameterInputImage("in",
                            m_ThresholdFunctorFilter->GetOutput());
//    
    ExecuteInternal("erode");
    
    
    GetInternalApplication("etape3")->SetParameterInputImage("inbmo",
                    GetInternalApplication("erode")->GetParameterOutputImage("out"));
    GetInternalApplication("etape3")->SetParameterInputImage("inlabels",
                    GetInternalApplication("etape1")->GetParameterOutputImage("out"));
    ExecuteInternal("etape3");
    

  }
  
  ThresholdFunctorFilterType::Pointer m_ThresholdFunctorFilter;
  

};


}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::SoilMoistureSegmentationFiltering)
