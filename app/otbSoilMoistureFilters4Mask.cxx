#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbWrapperCompositeApplication.h"
#include "itkMaskImageFilter.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
#include "otbHumidityFunctors.h"
#include "otbVectorDataIntoImageProjectionFilter.h"
#include "otbVectorDataToLabelImageFilter.h"
#include "otbConnectedLabelsImageFilter.h"

namespace otb
{
namespace Wrapper
{
class SoilMoistureFilters4Mask : public Application
{
public:
  typedef SoilMoistureFilters4Mask             Self;
  typedef Application                Superclass;
  typedef itk::SmartPointer<Self>    Pointer;

  itkNewMacro(Self);
  itkTypeMacro(SoilMoistureFilters4Mask, otb::Wrapper::Application);

  /** Pipeline */
  typedef UInt8ImageType MaskImageType;
  
  typedef itk::MaskImageFilter<FloatImageType,MaskImageType> MaskOnFloatType;
  typedef otb::ConnectedLabelsImageFilter<FloatImageType> DixPixFilterType;
  
  typedef otb::Functor::ThresholdFunctor<FloatImageType::PixelType,FloatImageType::PixelType> ThresholdFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatImageType,FloatImageType,ThresholdFunctorType> ThresholdFunctorFilterType; 
  

  typedef otb::Functor::MaskFunctor<FloatImageType::PixelType,MaskImageType::PixelType> MaskFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatImageType,MaskImageType,MaskFunctorType> MaskFunctorFilterType; 
  
//  typedef otb::Functor::SarAmplituteMaxFunctor<FloatImageType::PixelType,FloatImageType::PixelType,MaskImageType::PixelType> SarFunctorType;
//  typedef itk::BinaryFunctorImageFilter<FloatImageType,FloatImageType,MaskImageType,SarFunctorType> SarFunctorFilterType;

  
private:
  void DoInit()
  {
    SetName("SoilMoistureFilters4Mask");
    SetDescription("Apply eroded edge mask and remove cluster <20pix on NDVI segmented image");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Loïc Lozac'h");
    SetDocSeeAlso(" ");
    
    
    // Input images
    this->AddParameter(ParameterType_InputImage,"inbmo", "BinaryMorphologicalOperation result");
    SetParameterDescription                  ("inbmo", "BinaryMorphologicalOperation result");
    this->AddParameter(ParameterType_InputImage,"inlabels", "AgriMasked Labels");
    SetParameterDescription                  ("inlabels", "AgriMasked Labels");
//    this->AddParameter(ParameterType_InputImage,"sarvv", "Sentinel1 VV image");
//    SetParameterDescription                  ("sarvv", "Sentinel1 VV image");
//    this->AddParameter(ParameterType_InputImage,"sarth", "Sentinel1 Incidence image");
//    SetParameterDescription                  ("sarth", "Sentinel1 Incidence image");
    
    this->AddParameter(ParameterType_OutputImage,   "out", "Output image");
    SetParameterDescription                  ("out", "Masked NDVI segmented image");
    
    this->AddRAMParameter();
    

  }

  void DoUpdateParameters()
  {
  }

 

  /*
   * Do the work
   */
  void DoExecute()
  {
    

    /* Create mask from eroded thresholded edge and apply on masked segmentation */
    otbAppLogINFO("Create mask from eroded thresholded edge and apply on masked labels");
    m_MaskFunctorFilter = MaskFunctorFilterType::New();
    m_MaskFunctorFilter->SetInput(this->GetParameterFloatImage("inbmo"));
    m_MaskFunctorFilter->UpdateOutputInformation(); 
    
    m_MaskMaskFilter = MaskOnFloatType::New();
    m_MaskMaskFilter->SetInput1(this->GetParameterFloatImage("inlabels"));
    m_MaskMaskFilter->SetInput2(m_MaskFunctorFilter->GetOutput());
    m_MaskMaskFilter->UpdateOutputInformation();
  
    /* Fill 20pix holes */
    otbAppLogINFO("Remove cluster < 20pix");
    m_PixFilter = DixPixFilterType::New();
    m_PixFilter->SetInput(m_MaskMaskFilter->GetOutput());
    m_PixFilter->SetMinNumberOfComponents(20);
    m_PixFilter->UpdateOutputInformation();
    
//    m_SarFunctorFilter = SarFunctorFilterType::New();
//    m_SarFunctorFilter->SetInput1( GetParameterFloatImage("sarvv"));
//    m_SarFunctorFilter->SetInput2( GetParameterFloatImage("sarth"));
//    m_SarFunctorFilter->UpdateOutputInformation(); 
//    
//    m_MaskSarFilter = MaskOnFloatType::New();
//    m_MaskSarFilter->SetInput1(m_PixFilter->GetOutput());
//    m_MaskSarFilter->SetInput2(m_SarFunctorFilter->GetOutput());
//    m_MaskSarFilter->UpdateOutputInformation();
    
//
    SetParameterOutputImage("out", 
            m_PixFilter->GetOutput());

  }

  //Pipeline
  MaskOnFloatType::Pointer m_MaskMaskFilter;
  ThresholdFunctorFilterType::Pointer m_ThresholdFunctorFilter;
  MaskFunctorFilterType::Pointer m_MaskFunctorFilter;
  DixPixFilterType::Pointer m_PixFilter;
//  SarFunctorFilterType::Pointer m_SarFunctorFilter;
  MaskOnFloatType::Pointer m_MaskSarFilter;
  

};


}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::SoilMoistureFilters4Mask)
