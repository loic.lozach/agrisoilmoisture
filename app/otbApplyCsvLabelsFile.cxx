#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"

// Mapper
#include "otbHumidityFunctors.h"
#include "itkUnaryFunctorImageFilter.h"

// Export csv	
#include <boost/algorithm/string.hpp>
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <iterator>
#include <string>
#include <algorithm>
#include <math.h>

namespace otb
{
namespace Wrapper
{
class ApplyCsvLabelsFile : public Application
{
public:
  typedef ApplyCsvLabelsFile             Self;
  typedef Application                Superclass;
  typedef itk::SmartPointer<Self>    Pointer;

  itkNewMacro(Self);
  itkTypeMacro(ApplyCsvLabelsFile, Application);
  

  
  typedef std::map<UInt32ImageType::PixelType, FloatImageType::PixelType> OutputMapType;
  
  /** Soil moisture mapping */
  typedef otb::Functor::MapperFunctor<UInt32ImageType::PixelType, FloatImageType::PixelType, OutputMapType> MapperFunctorType;
  typedef itk::UnaryFunctorImageFilter<UInt32ImageType, FloatImageType, MapperFunctorType> MoistMapFilterType;
  
  

private:
  void DoInit()
  {
    SetName("ApplyCsvLabelsFile");
    SetDescription("Apply csv results from otbInvertSARModel on Labels image");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Loic Lozach");
    SetDocSeeAlso(" ");

    // Input images
    AddParameter(ParameterType_InputFilename,    "incsv",       "Input CSV File");
    AddParameter(ParameterType_InputImage,    "inlabels",    "Input labels image");
    
    // Output image
    AddParameter(ParameterType_OutputImage,   "out", "Output image");
    SetParameterDescription                  ("out", "The output image is the estimated soil moisture");
   

  }

  void DoUpdateParameters()
  {
    
  }

  /*
   * Do the work
   */
  void DoExecute()
  {
    std::ifstream file(this->GetParameterString("incsv"));

    OutputMapType dataList;
    UInt32ImageType::PixelType label;
    FloatImageType::PixelType moist;
    
    std::string line = "";
    // Iterate through each line and split the content using delimeter, pass header
    int i=0;
    while (getline(file, line))
    {
        i+=1;
        if(i==1) 
            continue;
                       
        std::vector<std::string> vec;
        boost::algorithm::split(vec, line, boost::is_any_of(";"));
        label = std::stoi(vec[0]);
        moist = std::stof(vec[5]);
        dataList.insert({label,moist});
    }
    // Close the File
    file.close();  
    
    otbAppLogINFO("Exporting to raster file");
    // Produce the map
    //moistMap[0] = 0 ;
    m_MapFilter = MoistMapFilterType::New();
    m_MapFilter->SetInput(GetParameterUInt32Image("inlabels"));
    m_MapFilter->GetFunctor().SetMap(dataList);


    SetParameterOutputImage("out", m_MapFilter->GetOutput());
    
  }


  // Create a nice map
  MoistMapFilterType::Pointer m_MapFilter;
  
};


}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::ApplyCsvLabelsFile)
        

