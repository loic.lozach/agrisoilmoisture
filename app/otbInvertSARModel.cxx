#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
//Stack
#include "otbTensorflowSource.h"
// Stats
//#include "otbStreamingStatisticsMapFromLabelImageFilter.h"
#include "otbStreamingStatisticsMapFromLabelImageFilter3.h"

// Tensorflow stuff
#include "tensorflow/core/public/session.h"
#include "tensorflow/core/platform/env.h"
#include "otbTensorflowGraphOperations.h"
#include "otbMoistureEstimator.h"

// Mapper
#include "otbHumidityFunctors.h"
#include "itkUnaryFunctorImageFilter.h"
#include "itkBinaryFunctorImageFilter.h"

// Export csv	
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <iterator>
#include <string>
#include <algorithm>
#include <math.h>

namespace otb
{
namespace Wrapper
{
class InvertSARModel : public Application
{
public:
  typedef InvertSARModel             Self;
  typedef Application                Superclass;
  typedef itk::SmartPointer<Self>    Pointer;

  itkNewMacro(Self);
  itkTypeMacro(InvertSARModel, Application);
  

  /** Typedefs for image concatenation */
  typedef TensorflowSource<FloatVectorImageType>                       TFSourceType;
  
  /** Statistics */
  typedef otb::StreamingStatisticsMapFromLabelImageFilter3<FloatVectorImageType, UInt32ImageType> StatsFilterType;
  typedef itk::ImageRegionIterator<FloatVectorImageType> IteratorType;
  typedef itk::ImageRegionConstIterator<FloatVectorImageType> ConstIteratorType;

  /** Inversion with TensorFlow */
  typedef StatsFilterType::PixelValueMapType InputMapType;
  typedef std::map<UInt32ImageType::PixelType, FloatImageType::PixelType> OutputMapType;
  typedef otb::MoistureEstimator<InputMapType, OutputMapType> EstimatorType;

  /** Soil moisture mapping */
  typedef otb::Functor::MapperFunctor<UInt32ImageType::PixelType, FloatImageType::PixelType, OutputMapType> MapperFunctorType;
  typedef itk::UnaryFunctorImageFilter<UInt32ImageType, FloatImageType, MapperFunctorType> MoistMapFilterType;
  
  /** Soil Moisture Filtering */
  
  typedef otb::Functor::SarAmplituteMaxFunctor<FloatVectorImageType::PixelType,FloatVectorImageType::PixelType> SarFunctorType;
  typedef itk::BinaryFunctorImageFilter<FloatVectorImageType,FloatVectorImageType,FloatVectorImageType,SarFunctorType> SarFunctorFilterType;
  
  typedef otb::Functor::SarVHAmplituteMaxFunctor<FloatVectorImageType::PixelType,FloatVectorImageType::PixelType> SarVHFunctorType;
  typedef itk::BinaryFunctorImageFilter<FloatVectorImageType,FloatVectorImageType,FloatVectorImageType,SarVHFunctorType> SarVHFunctorFilterType;
  
  typedef otb::Functor::NdviAndMoistFunctor<FloatVectorImageType::PixelType,FloatVectorImageType::PixelType> NdviAndMoistFunctorType;
  typedef itk::BinaryFunctorImageFilter<FloatVectorImageType,FloatVectorImageType,FloatVectorImageType,NdviAndMoistFunctorType> NdviAndMoistFunctorFilterType;
  

private:
  void DoInit()
  {
    SetName("InvertSARModel");
    SetDescription("Invert the IEM Model over SAR data");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Remi Cresson");
    SetDocSeeAlso(" ");

    // Input images
    AddParameter(ParameterType_InputImage,    "insarvv",       "Input SAR VV image");
    AddParameter(ParameterType_InputImage,    "insarth",       "Input SAR Theta image");
    AddParameter(ParameterType_InputImage,    "inndvi",       "Input NDVI  image");
    AddParameter(ParameterType_InputImage,    "inlabels",    "Input label image");
    AddParameter(ParameterType_Int,           "nodatalabel", "Input label image no-data value");
    SetDefaultParameterInt("nodatalabel", 0);
    
    
    AddParameter(ParameterType_Choice,"sarmode","set the SAR mode input image");
    SetParameterDescription("sarmode","Choice of the SAR mode input image");
    AddChoice("sarmode.vv","Choice of VV mode as SAR image input.");
    AddChoice("sarmode.vh","Choice of VH mode as SAR image input.");
    SetParameterString("sarmode","vv");
    MandatoryOff("sarmode");

    // Input model
    AddParameter(ParameterType_Group,         "model",       "Tensorflow model parameters");
    AddParameter(ParameterType_Directory,     "model.dir",   "TensorFlow SavedModel directory");
    MandatoryOn                              ("model.dir");
    SetParameterDescription                  ("model.dir",   "The model directory should contains the model Google Protobuf (.pb) and variables");

    AddParameter(ParameterType_Choice,"format","set the output format for invertion results");
    SetParameterDescription("format","Choice of the output format for invertion results");
    AddChoice("format.raster","Choice of a raster file as output.");
    AddChoice("format.csv","Choice of a csv file as output.");
        
    AddParameter(ParameterType_OutputFilename, "format.csv.out", "Output csv file");
    SetParameterDescription                  ("format.csv.out", "Export csv file containing labelid, sarvv, sarincidence, ndvi and moisture");
    // Output image
    AddParameter(ParameterType_OutputImage,   "format.raster.out", "Output image");
    SetParameterDescription                  ("format.raster.out", "The output image is the estimated soil moisture");
    m_ImageList = FloatVectorImageListType::New();

  }

  void DoUpdateParameters()
  {
    m_ImageList = FloatVectorImageListType::New();
  }

  /*
   * Remove the entry of the map corresponding to the no data label value
   */
  template<class TMap>
  void RemoveNoDataLabelFromMap(TMap& map)
  {
    int intNoData = GetParameterInt("nodatalabel");
    map.erase(intNoData);
  }
  
  template<class TMap>
  void RemoveNoDataLabelFromMap(TMap& map, std::vector<long> labelstoerase)
  {
    for (auto& lte: labelstoerase){
        map.erase(lte);
    }
  }
  
  

  /*
   * Do the work
   */
  void DoExecute()
  {
//      FloatVectorImageListType::Pointer list = GetParameterImageList("il");
    if (GetParameterString("sarmode") == "vv"){
        m_SarFunctorFilter = SarFunctorFilterType::New();
        m_SarFunctorFilter->SetInput1( GetParameterFloatVectorImage("insarvv"));
        m_SarFunctorFilter->SetInput2( GetParameterFloatVectorImage("insarth"));
        m_SarFunctorFilter->UpdateOutputInformation(); 
    }else {
        
        m_SarVHFunctorFilter = SarVHFunctorFilterType::New();
        m_SarVHFunctorFilter->SetInput1( GetParameterFloatVectorImage("insarvv"));
        m_SarVHFunctorFilter->SetInput2( GetParameterFloatVectorImage("insarth"));
        m_SarVHFunctorFilter->UpdateOutputInformation(); 
    }
    
    
    // Create a stack of input images
    
    m_ImageList->PushBack(GetParameterFloatVectorImage("insarth"));
    m_ImageList->PushBack(GetParameterFloatVectorImage("inndvi"));
    if (GetParameterString("sarmode") == "vv"){
        m_ImageList->PushBack(m_SarFunctorFilter->GetOutput());
    }else{
        m_ImageList->PushBack(m_SarVHFunctorFilter->GetOutput());
    }
    m_ImageSource.Set(m_ImageList);
      
    // Compute stats
    int intNoData = GetParameterInt("nodatalabel");
    std::cout << "Not Using Nodata = "  << std::to_string(intNoData) << " in statfilter" << std::endl;
    m_StatsFilter = StatsFilterType::New();
    m_StatsFilter->SetUseNoDataValue(true);
    m_StatsFilter->SetNoDataValue(intNoData);
    m_StatsFilter->SetInput(m_ImageSource.Get());
    m_StatsFilter->SetInputLabelImage(GetParameterUInt32Image("inlabels"));
    //m_StatsFilter->SETn
    AddProcess(m_StatsFilter->GetStreamer(), "Computing statistics");
    m_StatsFilter->Update();

    StatsFilterType::PixelValueMapType meanValues = m_StatsFilter->GetMeanValueMap();
    RemoveNoDataLabelFromMap<StatsFilterType::PixelValueMapType>(meanValues);
    otbAppLogINFO("Done (" << (meanValues.size()) << " objects)");
    
    //get count for each label
    StatsFilterType::LabelPopulationMapType countValues = m_StatsFilter->GetLabelPopulationMap();
    StatsFilterType::PixelValueMapType validRatioMap = m_StatsFilter->GetValidPixelsRatioMap();
    
//    for (const auto& entry: meanValues)
//      std::cout << "Id:105826152, value=" << meanValues.at(105826152)  << std::endl;
//      std::cout << "Id:105826152, validratio=" << validRatioMap.at(105826152)  << std::endl;
//      std::cout << "Id:105826152, pixparcelle=" << countValues.at(105826152)  << std::endl;

    // Put VV in dB
    std::vector<long> wronglabels;

    for (auto& entry: meanValues)
    {
        double nbsarpix = countValues.at(entry.first)*validRatioMap.at(entry.first)[2]; 
        if (validRatioMap.at(entry.first)[2] < 0.3 or entry.second[0] == 0 or entry.second[1] == 0 or entry.second[2] == 0
                or isnan(entry.second[0]) or isnan(entry.second[1]) or isnan(entry.second[2])){
            wronglabels.push_back(entry.first);
        }else{
           
//        otbAppLogINFO("Label = " << (entry.first) << ", vv = " << (entry.second[2]));
        //passage en decibels
            entry.second[2] = 10*std::log10(entry.second[2]);
            //apply bias NDVI*100
            entry.second[1] = (entry.second[1])/100;
    //        otbAppLogINFO("Log vv = " << (meanValues.at(entry.first)[2])); 
        }

    }
    //cleaning labels
    if (wronglabels.size() > 0)
        RemoveNoDataLabelFromMap<StatsFilterType::PixelValueMapType>(meanValues,wronglabels);
    
    //remove label stats in map
//    for (auto& entry: meanValues)
//    {
//        entry.second.SetSize(3,false);
//
//    }
  
    // Invert the model

    // 1. Load the Tensorflow bundle
    tf::LoadModel(GetParameterAsString("model.dir"), m_SavedModel);
    otbAppLogINFO("Load the Tensorflow bundle done");

    // 2. Run the estimator
    m_Estimator = EstimatorType::New();
    m_Estimator->SetSession(m_SavedModel.session.get());
    m_Estimator->SetGraph(m_SavedModel.meta_graph_def.graph_def());
    m_Estimator->SetInputMap(meanValues);
    AddProcess(m_Estimator, "Inverting model");
    m_Estimator->Update();
    OutputMapType moistMap = m_Estimator->GetOutputMap();
    
    // Filter results
    std::list< OutputMapType::iterator > iteratorList;
    
    for(OutputMapType::iterator it=moistMap.begin(); it!=moistMap.end(); ++it ){
        if ( it->second <= 10 and meanValues.at(it->first)[1] > 0.7){
            iteratorList.push_back(it);
        }
    }
    
    for(std::list< OutputMapType::iterator>::iterator it=iteratorList.begin(); it!=iteratorList.end(); ++it ){
        moistMap.erase(*it);
    }
//    moistMap.erase(std::remove_if(moistMap.begin(),
//                              moistMap.end(),
//                              [meanValues](OutputMapType::iterator v){return v->second <= 7 and meanValues.at(v->first)[1] > 0.7;}),
//               moistMap.end()); 
    
    if (GetParameterString("format") == "csv"){
        otbAppLogINFO("Exporting to csv file");
        	// Creating an object of CSVWriter
        std::fstream file;
        file.open(this->GetParameterString("format.csv.out"), std::ios::out | std::ios::trunc);
        
        std::vector<std::string> dataHeader = { "Labelid", "NbPixels", "SarIncidence", "NDVI","SarVV", "Moisture"};
        // Iterate over the range and add each lement to file seperated by delimeter.
        for (std::vector<std::string>::iterator ith=dataHeader.begin(); ith != dataHeader.end(); ++ith)
        {
                file << *ith;
                if (ith != dataHeader.end())
                        file << ";";
        }
        file << "\n";

            
        for(OutputMapType::iterator it=moistMap.begin(); it!=moistMap.end(); ++it ){
            // Adding vector to CSV File
            if ( isnan(meanValues.at(it->first)[0]) or isnan(meanValues.at(it->first)[1]) 
                    or isnan(meanValues.at(it->first)[2]) or isnan(it->second))
                continue;
            
            std::vector<std::string> dataList_1 = { std::to_string(it->first), std::to_string(countValues.at(it->first)), std::to_string(meanValues.at(it->first)[0]), 
                                                    std::to_string(meanValues.at(it->first)[1]),std::to_string(meanValues.at(it->first)[2]), 
                                                    std::to_string(it->second)};
            for (std::vector<std::string>::iterator itl=dataList_1.begin(); itl != dataList_1.end(); ++itl)
            {
                    file << *itl;
                    if (itl != dataList_1.end())
                            file << ";";
            }
            file << "\n";
        }
        // Close the file
        file.close();
 
    }
    else
    {
        otbAppLogINFO("Exporting to raster file");
        // Produce the map
        moistMap[0] = 0 ;
        m_MapFilter = MoistMapFilterType::New();
        m_MapFilter->SetInput(GetParameterUInt32Image("inlabels"));
        m_MapFilter->GetFunctor().SetMap(moistMap);


        SetParameterOutputImage("format.raster.out", m_MapFilter->GetOutput());
    }
  }

  // TensorFlow SavedModel (contains the Graph + the session)
  tensorflow::SavedModelBundle m_SavedModel; // must be alive during all the execution of the application !

  // Stack
  TFSourceType             m_ImageSource;
  // Compute a std::map<labels, S1 pixels>
  StatsFilterType::Pointer m_StatsFilter;

  // Moisture estimator
  EstimatorType::Pointer m_Estimator;

  // Create a nice map
  MoistMapFilterType::Pointer m_MapFilter;
  
  // Filter
  FloatVectorImageListType::Pointer m_ImageList;
  SarFunctorFilterType::Pointer m_SarFunctorFilter;
  SarVHFunctorFilterType::Pointer m_SarVHFunctorFilter;
};


}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::InvertSARModel)
        

