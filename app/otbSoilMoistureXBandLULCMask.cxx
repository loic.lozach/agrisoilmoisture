#include "otbWrapperApplication.h"
#include "otbWrapperApplicationFactory.h"
#include "otbWrapperCompositeApplication.h"
#include "itkFixedArray.h"
#include "itkObjectFactory.h"
#include "otbHumidityFunctors.h"
#include "itkMaskImageFilter.h"
// Resample filter
#include "otbGenericRSResampleImageFilter.h"
#include "itkNearestNeighborInterpolateImageFunction.h"
//Edge extract
#include "itkGradientMagnitudeImageFilter.h"
#include "otbMultiToMonoChannelExtractROI.h"
namespace otb
{
namespace Wrapper
{
class SoilMoistureXBandLULCMask : public Application
{
public:
  typedef SoilMoistureXBandLULCMask             Self;
  typedef Application                Superclass;
  typedef itk::SmartPointer<Self>    Pointer;

  itkNewMacro(Self);
  itkTypeMacro(SoilMoistureXBandLULCMask, otb::Wrapper::Application);

  /** Pipeline */
  typedef UInt8ImageType MaskImageType;
  /** Typedefs for image reprojection */
  typedef otb::GenericRSResampleImageFilter< FloatImageType, FloatImageType>     ResamplerType;
  typedef itk::NearestNeighborInterpolateImageFunction<FloatImageType, double>   NNInterpolatorType;
  
  typedef otb::Functor::MaskLULCFunctor<FloatImageType::PixelType,MaskImageType::PixelType> MaskLULCFunctorType;
  typedef itk::UnaryFunctorImageFilter<FloatImageType,MaskImageType,MaskLULCFunctorType> MaskLULCFilterType; 

  typedef itk::MaskImageFilter<FloatVectorImageType,MaskImageType,FloatVectorImageType> ApplyFloatMaskType;
  
private:
  void DoInit()
  {
    SetName("SoilMoistureXBandLULCMask");
    SetDescription("Apply Agricultural Mask on staked images from landcover image (default Theia OSO's Landcover");

    // Documentation
    SetDocLimitations("None");
    SetDocAuthors("Loïc Lozac'h");
    SetDocSeeAlso(" ");
    
    
    
    // Input images
    this->AddParameter(ParameterType_InputImage,"inr", "Image reference (Segmented NDVI)");
    SetParameterDescription                  ("inr", "Input NDVI image");
    this->AddParameter(ParameterType_InputImage,"inm", "LandUseLandCover (default Theia OSO's landuse)");
    SetParameterDescription                  ("inm", "Input LandUseLandCover");
    this->AddParameter(ParameterType_StringList,"agrival","Agricultural Landuse values (default Theia OSO = 11 12)");
    this->SetParameterStringList("agrival", {"11", "12"});
    
    this->AddParameter(ParameterType_OutputImage,   "out", "Agricultural masked labels image");
    SetParameterDescription                  ("out", "Agricultural masked labels image");
//    this->SetParameterOutputImagePixelType("out", ImagePixelType_uint32);
    
    this->AddRAMParameter();

  }

  void DoUpdateParameters()
  {
  }

 

  /*
   * Do the work
   */
  void DoExecute()
  {
      
    /*
     * Superimpose 
     */
    
    m_Resampler = ResamplerType::New();
    m_Resampler->SetInput(this->GetParameterFloatImage("inm"));

    // Setup transform through projRef and Keywordlist
    FloatImageType::SpacingType defSpacing = this->GetParameterFloatVectorImage("inr")->GetSignedSpacing();
    defSpacing[0] *= 10;
    defSpacing[1] *= 10;
    m_Resampler->SetDisplacementFieldSpacing(defSpacing);
    m_Resampler->SetInputKeywordList(this->GetParameterFloatImage("inm")->GetImageKeywordlist());
    m_Resampler->SetInputProjectionRef(this->GetParameterFloatImage("inm")->GetProjectionRef());
    m_Resampler->SetOutputKeywordList(this->GetParameterFloatVectorImage("inr")->GetImageKeywordlist());
    m_Resampler->SetOutputProjectionRef(this->GetParameterFloatVectorImage("inr")->GetProjectionRef());
    m_Resampler->SetOutputOrigin(this->GetParameterFloatVectorImage("inr")->GetOrigin());
    m_Resampler->SetOutputSpacing(this->GetParameterFloatVectorImage("inr")->GetSignedSpacing());
    m_Resampler->SetOutputSize(this->GetParameterFloatVectorImage("inr")->GetLargestPossibleRegion().GetSize());
    m_Resampler->SetOutputStartIndex(this->GetParameterFloatVectorImage("inr")->GetLargestPossibleRegion().GetIndex());

    typename NNInterpolatorType::Pointer interpolator = NNInterpolatorType::New();
    m_Resampler->SetInterpolator(interpolator);
    m_Resampler->UpdateOutputInformation();
    
    m_MaskLULCFilter = MaskLULCFilterType::New();
    m_MaskLULCFilter->SetInput(m_Resampler->GetOutput());
    m_MaskLULCFilter->GetFunctor().SetAgrivalues(GetParameterStringList("agrival"));
    m_MaskLULCFilter->UpdateOutputInformation();  
    
    m_MaskSegmFilter = ApplyFloatMaskType::New();
    m_MaskSegmFilter->SetInput1(this->GetParameterFloatVectorImage("inr"));
    m_MaskSegmFilter->SetInput2(m_MaskLULCFilter->GetOutput());
    m_MaskSegmFilter->UpdateOutputInformation();
//
    SetParameterOutputImage("out", 
            m_MaskSegmFilter->GetOutput());

  }

  //Pipeline
  ResamplerType::Pointer m_Resampler;
  MaskLULCFilterType::Pointer m_MaskLULCFilter;
  ApplyFloatMaskType::Pointer m_MaskSegmFilter;

};


}

}
OTB_APPLICATION_EXPORT(otb::Wrapper::SoilMoistureXBandLULCMask)
